--  Copyright 2021 TestyCool Authors
--  SPDX-License-Identifier: Apache-2.0
--
--  Licensed under the Apache License, Version 2.0 (the "License");
--  you may not use this file except in compliance with the License.
--  You may obtain a copy of the License at:
--
--    http://www.apache.org/licenses/LICENSE-2.0
--
--  Unless required by applicable law or agreed to in writing, software
--  distributed under the License is distributed on an "AS IS" BASIS,
--  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--  See the License for the specific language governing permissions and
--  limitations under the License.

pragma foreign_keys = on;

create table if not exists exams
(
    id         integer primary key,
    password   varchar(64) not null unique,
    title      varchar(64) not null,
    -- exam status
    --      1 - waiting
    --      2 - started
    --      3 - done
    status     integer     not null check (status in (1, 2, 3)) default 1,
    time_limit integer     null,
    start_at   integer     null
);

create index if not exists exams_pin_index on exams (password);

create table if not exists participants
(
    id      integer primary key,
    code    varchar(64) default ('default-' || lower(hex(randomblob(4)))),
    name    varchar(64) not null,
    exam_id integer     not null,

    unique (code, exam_id),

    constraint fk_exams
        foreign key (exam_id)
            references exams (id)
            on delete cascade
);

create index if not exists participants_code_index on participants (code);

create table if not exists questions
(
    id             integer primary key,
    -- question type
    --      1 - multiple choice
    --      2 - exam
    type           integer not null check (type in (1, 2)),
    -- question content format
    --      1 - plain text
    --      2 - html
    --      3 - markdown
    content_format integer not null check (content_format in (1, 2, 3)) default 1,
    content        text    not null,
    exam_id        integer not null,

    constraint fk_exams
        foreign key (exam_id)
            references exams (id)
            on delete cascade
);

create table if not exists choices
(
    id             integer primary key,
    is_correct     boolean not null check (is_correct in (0, 1))        default 0,
    -- choice content format
    --      1 - plain text
    --      2 - html
    --      3 - markdown
    content_format integer not null check (content_format in (1, 2, 3)) default 1,
    content        text    not null,
    question_id    integer not null,

    constraint fk_questions
        foreign key (question_id)
            references questions (id)
            on delete cascade
);

create table if not exists answers
(
    id               integer primary key,
    participant_id   integer not null,
    question_id      integer not null,

    unique (participant_id, question_id),

    constraint fk_participants
        foreign key (participant_id)
            references participants (id)
            on delete cascade,
    constraint fk_questions
        foreign key (question_id)
            references questions (id)
            on delete cascade
);

create table if not exists choice_answers
(
    answer_id integer primary key,
    choice_id integer not null,

    constraint fk_answers
        foreign key (answer_id)
            references answers (id)
            on delete cascade,
    constraint fk_choices
        foreign key (choice_id)
            references choices (id)
            on delete cascade
);

create table if not exists essay_answers
(
    answer_id      integer primary key,
    is_correct     boolean null check (is_correct in (0, 1))            default 0,
    -- essay answers content format
    --      1 - plain text
    --      2 - html
    --      3 - markdown
    content_format integer not null check (content_format in (1, 2, 3)) default 1,
    content        text    not null,

    constraint fk_answers
        foreign key (answer_id)
            references answers (id)
            on delete cascade
);

create table if not exists attempts
(
    id               integer primary key,
    participant_id   integer not null,
    exam_id          integer not null,
    finished         boolean not null default false,
    corrects         integer not null default 0,
    wrongs           integer not null default 0,
    unanswered       integer not null default 0,
    created_at       integer     null,
    updated_at       integer     null,

    unique (participant_id, exam_id),

    constraint fk_participants
        foreign key (participant_id)
            references participants (id)
            on delete cascade,
    constraint fk_exams
        foreign key (exam_id)
            references exams (id)
            on delete cascade
);

create table if not exists analytics
(
    id               integer primary key,
    participant_id   integer not null,
    exam_id          integer not null,
    message          varchar(64) not null,
    created_at       integer     null,

    constraint fk_participants
        foreign key (participant_id)
            references participants (id)
            on delete cascade,
    constraint fk_exams
        foreign key (exam_id)
            references exams (id)
            on delete cascade
);
