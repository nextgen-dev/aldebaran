MAIN_PACKAGE := gitlab.com/testycool/server/cmd/server

GO_VERSION := 1.18
GOLANGCI_LINT_VERSION := 1.42.0
MOCKGEN_VERSION := 1.6.0
SHELL := /bin/bash

.DEFAULT_GOAL := build
.PHONY: help setup setup-dev setup-check gen format lint test test-no-race build clean run snapshot release

help:	## Show this help.
	@grep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "%-20s%s\n", $$1, $$2}'

setup:	## Setup dependencies required to build this project. It will fail if manual intervention is required.
	@[[ `command -v go` ]] && [[ `go version | sed -rn 's/^.*version go([0-9.]+).*/\1/p' | grep "${GO_VERSION}"` != "" ]] \
		|| (echo "go version ${GO_VERSION}.x not found. Manual installation required. See https://golang.org/doc/install" && exit 1)
	@go install github.com/goreleaser/goreleaser@latest
	@go mod vendor
	@go mod verify

setup-dev: setup	## Setup dependencies and tools for development.
	@[[ `command -v protoc` ]] \
		|| (echo "protoc not found. Manual installation required. See https://grpc.io/docs/protoc-installation" && exit 1)
	@go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.1
	@go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.26
	@go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@latest
	@go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@latest
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@v${GOLANGCI_LINT_VERSION}
	@go install github.com/golang/mock/mockgen@v${MOCKGEN_VERSION}
	@go install golang.org/x/tools/cmd/goimports@latest

setup-check:	## Check development environment setup.
	@[[ `command -v go` ]] \
		&& [[ `go version | sed -rn 's/^.*version go([0-9.]+).*/\1/p' | grep "${GO_VERSION}"` != "" ]] \
		|| echo "go version ${GO_VERSION}.x not found. See https://golang.org/doc/install. (REQUIRED FOR BUILDING)"
	@[[ `command -v upx` ]] \
		|| echo "upx not found. See https://upx.github.io. (REQUIRED FOR PACKAGING)"
	@[[ `command -v protoc` ]] \
		|| echo "protoc not found. See https://grpc.io/docs/protoc-installation"
	@[[ `command -v golangci-lint` ]] \
		&& [[ `golangci-lint --version | sed -rn 's/^.*version v([0-9.]+).*/\1/p'` == "${GOLANGCI_LINT_VERSION}" ]] \
		|| echo "golangci-lint version ${GOLANGCI_LINT_VERSION} not found"
	@[[ `command -v mockgen` ]] \
		&& [[ `mockgen -version | sed -rn 's/^.*v([0-9.]+).*/\1/p'` == "${MOCKGEN_VERSION}" ]] \
		|| echo "mockgen version ${MOCKGEN_VERSION} not found"
	@[[ `command -v goimports` ]] || echo "goimports not found"
	@[[ `command -v goreleaser` ]] || echo "goreleaser not found"
	@[[ `command -v git` ]] || echo "git not found"
	@[[ `command -v docker` ]] || echo "docker not found"
	@[[ -d "vendor" ]] || (echo "module vendor directory does not exist")

gen:	## Generate files such as protobuf with protoc and files with //go:generate
	@protoc \
		--proto_path=api/protobuf \
		--go_out=api/protobuf/gen/go --go_opt=paths=source_relative \
    	--go-grpc_out=api/protobuf/gen/go --go-grpc_opt=paths=source_relative \
    	--grpc-gateway_out=api/protobuf/gen/go --grpc-gateway_opt=paths=source_relative  --grpc-gateway_opt=logtostderr=true \
    	--openapiv2_out=api/openapi --openapiv2_opt=logtostderr=true \
    	api/protobuf/testycool/*.proto
	@go generate ./...

format:	## Format all sources with gofmt and goimports.
	@go fmt $(shell go list ./...)
	@goimports -w $(shell go list -f {{.Dir}} ./... | grep -v /vendor/)

lint:	## Run linters.
	@golangci-lint run

test:	## Run tests with data race detection.
	@go test -race ./...

test-no-race:	## Run tests without data race detection.
	@go test ./...

build: clean	## Build development binary. Set GOOS and GOARCH for cross-compilation.
	@goreleaser build --id server-dev --rm-dist --snapshot --single-target

package:	## Build and package as snapshot release for single target. Set GOOS and GOARCH for cross-compilation.
	@[[ `command -v upx` ]] \
		|| (echo "upx not found. Manual installation required. See https://upx.github.io" && exit 1)
	@goreleaser build --id server --rm-dist --snapshot --single-target

snapshot:	## Build and package snapshot release for all targets.
	@[[ `command -v upx` ]] \
		|| (echo "upx not found. Manual installation required. See https://upx.github.io" && exit 1)
	@goreleaser release --rm-dist --snapshot

release:	## Build, package, and release new version of the project.
	@[[ `command -v upx` ]] \
		|| (echo "upx not found. Manual installation required. See https://upx.github.io" && exit 1)
	@goreleaser release --rm-dist

clean:	## Clean build and dist files.
	@rm -rf dist

run:	## Run the project for development with data race detection. Runtime data stored in 'tmp/data/'
	@GO111MODULE=on CGO_ENABLED=1 ADMIN_PASSCODE=dev-passcode \
		go run -race "${MAIN_PACKAGE}" --verbose --grpc-reflection --grpc-log-requests --data-dir tmp/data \
		|| true