#!/usr/bin/env bash

#
# Copyright 2021 TestyCool Authors
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

if [ -z "$1" ]
  then
    echo "No argument supplied";
    echo "Usage: ./copy-migrations.sh <target_dir>";
    exit 1;
fi

target_dir="$(pwd)/${1}";

[[ -d "${target_dir}" ]] || mkdir -p "${target_dir}";

# Copy migrations to target directory
cp -r "$(dirname "${0}")"/../db/migrations/*.sql "${target_dir}";

# Strip comments
sed -i -e '/^\s*--/d;' "${target_dir}"/*.sql;

# Strip whitespaces
sed -i -z 's/\n/ /g; s/^[ \t]*//g; s/ \{2,\}/ /g' "${target_dir}"/*.sql;

# Add auto-generated header
current_time="$(date --utc --iso-8601=seconds)";
sed -i "1s;^;-- DO NOT EDIT! Code is auto-generated. Generated at: ${current_time}\n;" "${target_dir}"/*.sql;
