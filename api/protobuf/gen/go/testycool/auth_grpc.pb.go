// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package testycool

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// AuthServiceClient is the client API for AuthService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AuthServiceClient interface {
	GetAdminToken(ctx context.Context, in *GetAdminTokenRequest, opts ...grpc.CallOption) (*GetAdminTokenResponse, error)
	GetParticipantToken(ctx context.Context, in *GetParticipantTokenRequest, opts ...grpc.CallOption) (*GetParticipantTokenResponse, error)
}

type authServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewAuthServiceClient(cc grpc.ClientConnInterface) AuthServiceClient {
	return &authServiceClient{cc}
}

func (c *authServiceClient) GetAdminToken(ctx context.Context, in *GetAdminTokenRequest, opts ...grpc.CallOption) (*GetAdminTokenResponse, error) {
	out := new(GetAdminTokenResponse)
	err := c.cc.Invoke(ctx, "/testycool.v1.AuthService/GetAdminToken", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *authServiceClient) GetParticipantToken(ctx context.Context, in *GetParticipantTokenRequest, opts ...grpc.CallOption) (*GetParticipantTokenResponse, error) {
	out := new(GetParticipantTokenResponse)
	err := c.cc.Invoke(ctx, "/testycool.v1.AuthService/GetParticipantToken", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AuthServiceServer is the server API for AuthService service.
// All implementations must embed UnimplementedAuthServiceServer
// for forward compatibility
type AuthServiceServer interface {
	GetAdminToken(context.Context, *GetAdminTokenRequest) (*GetAdminTokenResponse, error)
	GetParticipantToken(context.Context, *GetParticipantTokenRequest) (*GetParticipantTokenResponse, error)
	mustEmbedUnimplementedAuthServiceServer()
}

// UnimplementedAuthServiceServer must be embedded to have forward compatible implementations.
type UnimplementedAuthServiceServer struct {
}

func (UnimplementedAuthServiceServer) GetAdminToken(context.Context, *GetAdminTokenRequest) (*GetAdminTokenResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAdminToken not implemented")
}
func (UnimplementedAuthServiceServer) GetParticipantToken(context.Context, *GetParticipantTokenRequest) (*GetParticipantTokenResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetParticipantToken not implemented")
}
func (UnimplementedAuthServiceServer) mustEmbedUnimplementedAuthServiceServer() {}

// UnsafeAuthServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AuthServiceServer will
// result in compilation errors.
type UnsafeAuthServiceServer interface {
	mustEmbedUnimplementedAuthServiceServer()
}

func RegisterAuthServiceServer(s grpc.ServiceRegistrar, srv AuthServiceServer) {
	s.RegisterService(&AuthService_ServiceDesc, srv)
}

func _AuthService_GetAdminToken_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAdminTokenRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthServiceServer).GetAdminToken(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/testycool.v1.AuthService/GetAdminToken",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthServiceServer).GetAdminToken(ctx, req.(*GetAdminTokenRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AuthService_GetParticipantToken_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetParticipantTokenRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthServiceServer).GetParticipantToken(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/testycool.v1.AuthService/GetParticipantToken",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthServiceServer).GetParticipantToken(ctx, req.(*GetParticipantTokenRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// AuthService_ServiceDesc is the grpc.ServiceDesc for AuthService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var AuthService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "testycool.v1.AuthService",
	HandlerType: (*AuthServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetAdminToken",
			Handler:    _AuthService_GetAdminToken_Handler,
		},
		{
			MethodName: "GetParticipantToken",
			Handler:    _AuthService_GetParticipantToken_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "testycool/auth.proto",
}
