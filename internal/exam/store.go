// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package exam

import (
	"context"
)

type Store interface {
	Save(ctx context.Context, exam Exam) (Exam, error)
	GetByID(ctx context.Context, id int32) (Exam, error)
	GetByPassword(ctx context.Context, password string) (Exam, error)
	ListAll(ctx context.Context) ([]Exam, error)
	ListPaginated(ctx context.Context, offset int32, limit int32) ([]Exam, error)
	GetTotalSize(ctx context.Context) (int32, error)
	Update(ctx context.Context, exam Exam) (Exam, error)
	Delete(ctx context.Context, id int32) error
}
