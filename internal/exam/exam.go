// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package exam

import (
	"errors"
	"time"
)

type Status int8

const (
	StatusUnknown Status = iota
	StatusWaiting
	StatusStarted
	StatusDone
)

var (
	ErrZeroStartTime = errors.New("start time is zero")
	ErrZeroTimeLimit = errors.New("time limit is zero seconds")
	ErrNoPassword    = errors.New("password is empty")
)

type Exam struct {
	ID        int32
	Password  string
	Title     string
	TimeLimit time.Duration
	StartAt   time.Time
	Status    Status
}

func (e *Exam) Validate() error {
	if e.StartAt.IsZero() {
		return ErrZeroStartTime
	}
	if e.TimeLimit.Seconds() == 0 {
		return ErrZeroTimeLimit
	}
	if e.Password == "" {
		return ErrNoPassword
	}
	return nil
}
