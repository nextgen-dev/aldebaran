// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package attempt

import (
	"errors"
	"fmt"
	"time"
)

var (
	ErrInvalidID     = errors.New("invalid attempt id")
	ErrZeroCreatedAt = errors.New("created time is zero")
	ErrZeroUpdatedAt = errors.New("updated time is zero")
)

type Attempt struct {
	ID            int32     `json:"id,omitempty"`
	Finished      bool      `json:"finished"`
	Corrects      int32     `json:"correct"`
	Wrongs        int32     `json:"wrong"`
	Unanswered    int32     `json:"unanswered"`
	ParticipantID int32     `json:"participant_id,omitempty"`
	ExamID        int32     `json:"exam_id,omitempty"`
	CreatedAt     time.Time `json:"created_at,omitempty"`
	UpdatedAt     time.Time `json:"updated_at,omitempty"`
}

func (a *Attempt) Validate() error {
	if a.ID < 0 {
		return ErrInvalidID
	}

	if a.ParticipantID < 0 {
		return ErrInvalidID
	}

	if a.ExamID < 0 {
		return ErrInvalidID
	}

	if a.CreatedAt.IsZero() {
		return ErrZeroCreatedAt
	}

	if a.UpdatedAt.IsZero() {
		return ErrZeroUpdatedAt
	}

	return nil
}

func (a *Attempt) ValidateNew() error {
	if a.ParticipantID < 0 {
		return ErrInvalidID
	}

	if a.ExamID < 0 {
		return ErrInvalidID
	}

	if a.CreatedAt.IsZero() {
		return ErrZeroCreatedAt
	}

	if a.UpdatedAt.IsZero() {
		return ErrZeroUpdatedAt
	}

	return nil
}

// Filter is a filter for listing questions.
type Filter struct {
	ParticipantID int32 `json:"participant_id,omitempty"`
	ExamID        int32 `json:"exam_id,omitempty"`
}

// ListOptions is a list options for listing questions with pagination.
type ListOptions struct {
	Filter Filter `json:"filter,omitempty"`
	Size   int32  `json:"size,omitempty"`
	Page   int32  `json:"limit,omitempty"`
}

func (o *ListOptions) Validate() error {
	if o.Size < -1 {
		return fmt.Errorf("invalid page size (%d < -1)", o.Size)
	}

	if o.Page < 1 {
		return fmt.Errorf("invalid page number (%d < 1)", o.Page)
	}

	return nil
}

// ListData is the data returned by the list operation.
type ListData struct {
	// Questions is the list of questions returned from the operation.
	Attempts []Attempt `json:"attempts,omitempty"`
	// Total is the total number of questions available in the store.
	TotalSize int32 `json:"total_size,omitempty"`
}
