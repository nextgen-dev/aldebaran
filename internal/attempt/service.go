// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package attempt

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/apperror"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/cachestore"
	"gitlab.com/testycool/server/internal/datastore"
)

type Service interface {
	Save(ctx context.Context, a Attempt) (Attempt, error)
	GetByID(ctx context.Context, id int32) (Attempt, error)
	GetByParticipantID(ctx context.Context, participantId int32) (Attempt, error)
	List(ctx context.Context, opts ListOptions) (ListData, error)
	Update(ctx context.Context, a Attempt) (Attempt, error)
	Delete(ctx context.Context, id int32) error
}

type service struct {
	attemptStore Store
	logger       *logrus.Entry
	singleCache  cachestore.Cache
	listCache    cachestore.Cache
}

func (s *service) Save(ctx context.Context, a Attempt) (Attempt, error) {
	uid, err := auth.GetUserID(ctx)
	if err != nil {
		return Attempt{}, apperror.Unauthenticated("authentication required to create new attempt", err)
	}

	if uid != auth.AdminUserID {
		a.ParticipantID = uid
	}

	err = a.ValidateNew()
	if err != nil {
		return Attempt{}, apperror.InvalidData("new attempt data is invalid: "+err.Error(), err)
	}

	s.listCache.Flush()

	a, err = s.attemptStore.Save(ctx, a)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Attempt{}, apperror.Internal("unable to get newly created attempt", err)
		}
		return Attempt{}, apperror.Internal("unable to save attempt", err)
	}

	cacheKey := fmt.Sprintf("id-%d", a.ID)
	s.singleCache.Set(cacheKey, a)

	return a, nil
}

func (s *service) GetByID(ctx context.Context, id int32) (Attempt, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return Attempt{}, apperror.Unauthenticated("authentication required to get attempt data", err)
	}

	cacheKey := fmt.Sprintf("id-%d", id)
	if cached, found := s.singleCache.Get(cacheKey); found {
		return cached.(Attempt), nil
	}

	a, err := s.attemptStore.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Attempt{}, apperror.NotFound("attempt not found", err)
		}
		return Attempt{}, apperror.Internal("unable to get attempt", err)
	}

	s.singleCache.Set(cacheKey, a)

	return a, nil
}

func (s *service) GetByParticipantID(ctx context.Context, participantId int32) (Attempt, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return Attempt{}, apperror.Unauthenticated("authentication required to get attempt data", err)
	}

	cacheKey := fmt.Sprintf("id-%d", participantId)
	if cached, found := s.singleCache.Get(cacheKey); found {
		return cached.(Attempt), nil
	}

	a, err := s.attemptStore.GetByParticipantID(ctx, participantId)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Attempt{}, apperror.NotFound("attempt not found", err)
		}
		return Attempt{}, apperror.Internal("unable to get attempt", err)
	}

	s.singleCache.Set(cacheKey, a)

	return a, nil
}

func (s *service) List(ctx context.Context, opts ListOptions) (ListData, error) {
	uid, err := auth.GetUserID(ctx)
	if err != nil {
		return ListData{}, apperror.Unauthenticated("authentication required to list attempts", err)
	}

	if uid != auth.AdminUserID {
		opts.Filter.ParticipantID = uid
	}

	err = opts.Validate()
	if err != nil {
		return ListData{}, apperror.OutOfRange("invalid list options: "+err.Error(), err)
	}

	cacheKey := fmt.Sprintf("list-%v", opts)
	if cached, found := s.listCache.Get(cacheKey); found {
		return cached.(ListData), nil
	}

	var offset, limit int32
	if opts.Size == -1 {
		offset = 0
		limit = 0
	} else {
		offset = opts.Size * (opts.Page - 1)
		limit = opts.Size
	}

	aa, err := s.attemptStore.List(ctx, offset, limit, opts.Filter)
	if err != nil {
		return ListData{}, apperror.Internal("unable to list attempts", err)
	}

	if len(aa) == 0 {
		aa = make([]Attempt, 0)
	}

	totalSize, err := s.attemptStore.GetTotalSize(ctx, opts.Filter)
	if err != nil {
		return ListData{}, apperror.Internal("unable to get total size while getting attempt list", err)
	}

	listData := ListData{
		Attempts:  aa,
		TotalSize: totalSize,
	}

	s.listCache.Set(cacheKey, listData)

	return listData, nil
}

func (s *service) Update(ctx context.Context, a Attempt) (Attempt, error) {
	uid, err := auth.GetUserID(ctx)
	if err != nil {
		return Attempt{}, apperror.Unauthenticated("admin access is required to update attempt", err)
	}

	if uid != auth.AdminUserID {
		a.ParticipantID = uid
	}

	s.listCache.Flush()

	a, err = s.attemptStore.Update(ctx, a)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("attempt with id = %d does not exist", a.ID)
			return Attempt{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to update attempt with id = %d", a.ID)
		return Attempt{}, apperror.Internal(msg, err)
	}

	cacheKey := fmt.Sprintf("id-%d", a.ID)
	s.singleCache.Set(cacheKey, a)

	return a, nil
}

func (s *service) Delete(ctx context.Context, id int32) error {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return apperror.Unauthorized("admin access is required to delete attempt", err)
	}

	cacheKey := fmt.Sprintf("id-%d", id)
	s.singleCache.Delete(cacheKey)
	s.listCache.Flush()

	err = s.attemptStore.Delete(ctx, id)
	if err != nil {
		msg := fmt.Sprintf("unable to delete attempt with id = %d", id)
		return apperror.Internal(msg, err)
	}

	return nil
}

func NewService(logger *logrus.Entry, attemptStore Store) (Service, error) {
	return &service{
		attemptStore: attemptStore,
		logger:       logger,
		singleCache:  cachestore.NewNoopCache(),
		listCache:    cachestore.NewNoopCache(),
	}, nil
}
