// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package attempt

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"zombiezen.com/go/sqlite"
	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/datastore"
)

type sqliteStore struct {
	name   string
	logger *logrus.Entry
	dbPool *sqlitex.Pool
}

func (s *sqliteStore) Save(ctx context.Context, a Attempt) (Attempt, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into attempts (participant_id, exam_id, finished, corrects, wrongs, unanswered, created_at, updated_at)" +
		" values ($participant_id, $exam_id, $finished, $corrects, $wrongs, $unanswered, $created_at, $updated_at)" +
		" returning id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Attempt{}, datastore.SaveError(s.name, "unable to prepare statement", err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$participant_id", int64(a.ParticipantID))
	stmt.SetInt64("$exam_id", int64(a.ExamID))
	stmt.SetBool("$finished", a.Finished)
	stmt.SetInt64("$corrects", int64(a.Corrects))
	stmt.SetInt64("$wrongs", int64(a.Wrongs))
	stmt.SetInt64("$unanswered", int64(a.Unanswered))
	stmt.SetInt64("$created_at", a.CreatedAt.Unix())
	stmt.SetInt64("$updated_at", a.CreatedAt.Unix())

	hasRow, err := stmt.Step()
	if err != nil {
		return Attempt{}, datastore.SaveError(s.name, "unable to execute statement", err)
	}
	if !hasRow {
		s.resetAndClearStmt(stmt)
		return Attempt{}, datastore.SaveError(s.name, "unable to get saved data", datastore.ErrNoRows)
	}

	a.ID = stmt.ColumnInt32(0)

	return a, nil
}

func (s *sqliteStore) GetByID(ctx context.Context, id int32) (Attempt, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, participant_id, exam_id, finished, corrects, wrongs, unanswered, created_at, updated_at from attempts" +
		" where id = $id limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Attempt{}, fmt.Errorf("attempt sqliteStore: unable to get attempt with id %d: %w", id, err)
	}
	stmt.SetInt64("$id", int64(id))

	hasRow, err := stmt.Step()
	if err != nil {
		return Attempt{}, fmt.Errorf("attempt sqliteStore: unable to get attempt with id %d: %w", id, err)
	}
	if !hasRow {
		return Attempt{}, fmt.Errorf(
			"attempt sqliteStore: unable to get attempt with id %d: %w",
			id,
			datastore.ErrNoRows,
		)
	}

	attempt := s.attemptFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Attempt{}, fmt.Errorf("attempt sqliteStore: unable to get attempt with id %d: %w", id, err)
	}

	return attempt, nil
}

func (s *sqliteStore) GetByParticipantID(ctx context.Context, participantId int32) (Attempt, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, participant_id, exam_id, finished, corrects, wrongs, created_at, updated_at, unanswered from attempts" +
		" where participant_id = $participant_id limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Attempt{}, fmt.Errorf(
			"attempt sqliteStore: unable to get attempt with participant_id '%d': %w",
			participantId,
			err,
		)
	}
	stmt.SetInt64("$participant_id", int64(participantId))

	hasRow, err := stmt.Step()
	if err != nil {
		return Attempt{}, fmt.Errorf(
			"attempt sqliteStore: unable to get attempt with participant_id '%d': %w",
			participantId,
			err,
		)
	}
	if !hasRow {
		return Attempt{}, fmt.Errorf(
			"attempt sqliteStore: unable to get attempt with id '%d': %w",
			participantId,
			datastore.ErrNoRows,
		)
	}

	attempt := s.attemptFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Attempt{}, fmt.Errorf(
			"attempt sqliteStore: unable to get attempt with id '%d': %w",
			participantId,
			datastore.ErrNoRows,
		)
	}

	return attempt, nil
}

func (s *sqliteStore) List(ctx context.Context, offset int32, limit int32, f Filter) ([]Attempt, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	stmt, err := s.buildListStmt(conn, offset, limit, f)
	if err != nil {
		return nil, err
	}
	defer s.resetAndClearStmt(stmt)

	var aa []Attempt
	var hasRow bool
	for {
		hasRow, err = stmt.Step()
		if err != nil || !hasRow {
			break
		}
		aa = append(aa, s.attemptFromStmt(stmt))
	}
	if err != nil {
		return nil, datastore.ListError(s.name, "unable to execute statement", offset, limit, f, err)
	}

	return aa, nil
}

func (s *sqliteStore) GetTotalSize(ctx context.Context, f Filter) (int32, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	stmt, err := s.buildTotalSizeStmt(conn, f)
	if err != nil {
		return 0, err
	}
	defer s.resetAndClearStmt(stmt)

	hasRow, err := stmt.Step()
	if err != nil {
		return 0, datastore.TotalSizeError(s.name, "unable to execute statement", f, err)
	}
	if !hasRow {
		return 0, datastore.TotalSizeError(s.name, "unable to get total size", f, datastore.ErrNoRows)
	}

	totalSize := stmt.GetInt64("total_size")

	return int32(totalSize), nil
}

func (s *sqliteStore) Update(ctx context.Context, a Attempt) (Attempt, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "update attempts set" +
		" finished = $finished, corrects = $corrects, wrongs = $wrongs, unanswered = $unanswered, created_at = $created_at, updated_at = $updated_at" +
		" where id = $id" +
		" returning id, participant_id, exam_id, finished, corrects, wrongs, unanswered, created_at, updated_at;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Attempt{}, fmt.Errorf("attempt sqliteStore: unable to update attempt with id %d: %w", a.ID, err)
	}

	stmt.SetBool("$finished", a.Finished)
	stmt.SetInt64("$corrects", int64(a.Corrects))
	stmt.SetInt64("$wrongs", int64(a.Wrongs))
	stmt.SetInt64("$unanswered", int64(a.Unanswered))
	stmt.SetInt64("$id", int64(a.ID))
	stmt.SetInt64("$created_at", a.CreatedAt.Unix())
	stmt.SetInt64("$updated_at", a.UpdatedAt.Unix())

	hasRow, err := stmt.Step()
	if err != nil {
		if code := sqlite.ErrCode(err); code == sqlite.ResultConstraintUnique {
			return Attempt{}, datastore.ErrNotUnique
		}
		return Attempt{}, fmt.Errorf("attempt sqliteStore: unable to update attempt with id %d: %w", a.ID, err)
	}
	if !hasRow {
		return Attempt{}, fmt.Errorf(
			"attempt sqliteStore: unable to get updated attempt with id %d: %w",
			a.ID,
			datastore.ErrNoRows,
		)
	}

	a = s.attemptFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Attempt{}, fmt.Errorf(
			"attempt sqliteStore: unable to finalize update for id %d: %w",
			a.ID,
			err,
		)
	}

	return a, nil
}

func (s *sqliteStore) Delete(ctx context.Context, id int32) error {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "delete from attempts where id = $id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return datastore.DeleteError(s.name, "unable to prepare statement", id, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$id", int64(id))

	_, err = stmt.Step()
	if err != nil {
		return datastore.DeleteError(s.name, "unable to execute statement", id, err)
	}

	return nil
}

func (s *sqliteStore) resetAndClearStmt(stmt *sqlite.Stmt) {
	err := stmt.Reset()
	if err != nil {
		s.logger.WithError(err).Error("unable to reset statement")
	}

	err = stmt.ClearBindings()
	if err != nil {
		s.logger.WithError(err).Error("unable to clear statement bindings")
	}
}

func (s *sqliteStore) attemptFromStmt(stmt *sqlite.Stmt) Attempt {
	a := Attempt{
		ID:            int32(stmt.GetInt64("id")),
		ParticipantID: int32(stmt.GetInt64("participant_id")),
		ExamID:        int32(stmt.GetInt64("exam_id")),
		Finished:      bool(stmt.GetInt64("finished") != 0),
		Corrects:      int32(stmt.GetInt64("corrects")),
		Wrongs:        int32(stmt.GetInt64("wrongs")),
		Unanswered:    int32(stmt.GetInt64("unanswered")),
		CreatedAt:     time.Unix(stmt.GetInt64("created_at"), 0),
		UpdatedAt:     time.Unix(stmt.GetInt64("updated_at"), 0),
	}

	return a
}

func (s *sqliteStore) buildListStmt(conn *sqlite.Conn, offset int32, limit int32, f Filter) (*sqlite.Stmt, error) {
	query := "select id, participant_id, exam_id, finished, corrects, wrongs, created_at, updated_at, unanswered from attempts"

	if f.ExamID != 0 {
		query += " where attempts.exam_id = $exam_id"
	}
	if limit != 0 {
		query += " limit $limit"
	}
	if offset != 0 {
		if limit == 0 {
			query += " limit -1"
		}
		query += " offset $offset"
	}

	query += ";"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, datastore.ListError(s.name, "unable to prepare statement", offset, limit, f, err)
	}

	if f.ExamID != 0 {
		stmt.SetInt64("$exam_id", int64(f.ExamID))
	}
	if offset != 0 {
		stmt.SetInt64("$offset", int64(offset))
	}
	if limit != 0 {
		stmt.SetInt64("$limit", int64(limit))
	}

	return stmt, nil
}

func (s *sqliteStore) buildTotalSizeStmt(conn *sqlite.Conn, f Filter) (*sqlite.Stmt, error) {
	query := "select count(*) as total_size from attempts"

	if f.ExamID != 0 {
		query += " where attempts.exam_id = $exam_id"
	}

	query += ";"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, datastore.TotalSizeError(s.name, "unable to prepare statement", f, err)
	}

	if f.ExamID != 0 {
		stmt.SetInt64("$exam_id", int64(f.ExamID))
	}

	return stmt, nil
}

func NewSQLiteStore(logger *logrus.Logger, dbPool *sqlitex.Pool) (Store, error) {
	const name = "attempt sqlite"
	return &sqliteStore{
		name:   name,
		logger: logger.WithField("store", name),
		dbPool: dbPool,
	}, nil
}
