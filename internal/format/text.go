// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package format

type Text int8

const (
	TextUnknown Text = iota
	TextPlain
	TextHTML
	TextMarkdown
)

func (t Text) String() string {
	switch t {
	case TextPlain:
		return "PLAIN"
	case TextHTML:
		return "HTML"
	case TextMarkdown:
		return "MARKDOWN"
	default:
		return "UNKNOWN"
	}
}

func (t Text) Valid() bool {
	switch t {
	case TextPlain, TextHTML, TextMarkdown:
		return true
	default:
		return false
	}
}
