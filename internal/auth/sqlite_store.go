// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/datastore"
)

type sqliteStore struct {
	logger   *logrus.Logger
	dbPool   *sqlitex.Pool
	passcode string
}

func (s *sqliteStore) GetAdminPasscode(_ context.Context) (string, error) {
	return s.passcode, nil
}

func (s *sqliteStore) CheckExamParticipant(ctx context.Context, examPassword string, participantCode string) (int32, int32, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select exams.id as exam_id, participants.id as participant_id from exams" +
		" join participants on participants.exam_id = exams.id" +
		" where exams.password = $exam_password and participants.code = $participant_code" +
		" limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return 0, 0, fmt.Errorf("auth sqliteStore: unable to prepare statement: %w", err)
	}
	stmt.SetText("$exam_password", examPassword)
	stmt.SetText("$participant_code", participantCode)

	hasRow, err := stmt.Step()
	if err != nil {
		return 0, 0, fmt.Errorf("auth sqliteStore: unable to execute query: %w", err)
	}
	if !hasRow {
		return 0, 0, fmt.Errorf("auth sqliteStore: no exam or participant found: %w", datastore.ErrNoRows)
	}

	participantID := int32(stmt.GetInt64("participant_id"))
	examID := int32(stmt.GetInt64("exam_id"))

	err = stmt.Finalize()
	if err != nil {
		return 0, 0, fmt.Errorf("auth sqliteStore: unable to finalize statement: %w", err)
	}

	return participantID, examID, nil
}

func NewSQLiteStore(logger *logrus.Logger, dbPool *sqlitex.Pool, passcode string) (Store, error) {
	return &sqliteStore{
		logger:   logger,
		dbPool:   dbPool,
		passcode: passcode,
	}, nil
}
