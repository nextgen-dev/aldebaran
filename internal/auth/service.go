// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"context"
	"errors"

	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/apperror"
	"gitlab.com/testycool/server/internal/datastore"
	"gitlab.com/testycool/server/internal/jwt"
)

const (
	AdminUserID int32 = -1
	AdminExamID int32 = -1
)

type Service interface {
	NewAdminToken(ctx context.Context, passcode string) (*Token, error)
	NewParticipantToken(ctx context.Context, examPassword string, participantCode string) (*Token, error)
}

type service struct {
	logger          *logrus.Logger
	jwtService      jwt.Service
	credentialStore Store
}

func (s *service) NewAdminToken(ctx context.Context, passcode string) (*Token, error) {
	p, err := s.credentialStore.GetAdminPasscode(ctx)
	if err != nil {
		return nil, apperror.Internal("unable to verify credential", err)
	}
	if passcode != p {
		//nolint:goerr113 // wrapped in apperror
		return nil, apperror.Unauthenticated("invalid credential", nil)
	}

	accessToken, err := s.jwtService.GenerateAndSign(true, AdminUserID, AdminExamID)
	if err != nil {
		return nil, apperror.Internal("unable to generate new access token", err)
	}

	return &Token{
		AccessToken: accessToken,
	}, nil
}

func (s *service) NewParticipantToken(
	ctx context.Context,
	examPassword string,
	participantCode string,
) (*Token, error) {
	participantID, examID, err := s.credentialStore.CheckExamParticipant(ctx, examPassword, participantCode)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return nil, apperror.Unauthenticated("invalid credentials", nil)
		}
		return nil, apperror.Internal("unable to verify credentials", err)
	}

	accessToken, err := s.jwtService.GenerateAndSign(false, participantID, examID)
	if err != nil {
		return nil, apperror.Internal("unable to generate new access token", err)
	}

	return &Token{
		AccessToken: accessToken,
	}, nil
}

func NewService(logger *logrus.Logger, jwtService jwt.Service, credentialStore Store) (Service, error) {
	return &service{
		logger:          logger,
		jwtService:      jwtService,
		credentialStore: credentialStore,
	}, nil
}
