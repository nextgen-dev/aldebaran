// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package version contains build-time information about the current binary.
// Variables under this package are assigned during build-time using linker flag.
package version

var (
	// Version is the version number of current build.
	Version string

	// GitSHA is the Git commit SHA of the current build.
	GitSHA string

	// Branch is the git branch name of the current build.
	Branch string

	// BuildTimestamp is the timestamp of the build.
	// Timestamp is formatted in RFC3339 format.
	// Example:
	// 	1990-12-31T23:59:60Z
	BuildTimestamp string
)
