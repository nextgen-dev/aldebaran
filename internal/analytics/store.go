// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package analytics

import (
	"context"
)

// Store is the interface for storing analytics that must be implemented by a storage system.
type Store interface {
	Save(ctx context.Context, p Analytics) (Analytics, error)
	GetByID(ctx context.Context, id int32) (Analytics, error)
	ListAll(ctx context.Context) ([]Analytics, error)
	ListAllByExamID(ctx context.Context, examID int32) ([]Analytics, error)
	ListPaginated(ctx context.Context, offset int32, limit int32) ([]Analytics, error)
	GetTotalSize(ctx context.Context) (int32, error)
	Update(ctx context.Context, p Analytics) (Analytics, error)
	Delete(ctx context.Context, id int32) error
}
