// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package answer

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/apperror"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/cachestore"
	"gitlab.com/testycool/server/internal/datastore"
)

type Service interface {
	Save(ctx context.Context, a Answer) (Answer, error)
	GetByID(ctx context.Context, id int32) (Answer, error)
	List(ctx context.Context, opts ListOptions) (ListData, error)
	Update(ctx context.Context, a Answer) (Answer, error)
	Delete(ctx context.Context, id int32) error
}

type service struct {
	answerStore Store
	logger      *logrus.Entry
	singleCache cachestore.Cache
	listCache   cachestore.Cache
}

func (s *service) Save(ctx context.Context, a Answer) (Answer, error) {
	uid, err := auth.GetUserID(ctx)
	if err != nil {
		return Answer{}, apperror.Unauthenticated("authentication required to create new answer", err)
	}

	if uid != auth.AdminUserID {
		a.ParticipantID = uid
	}

	err = a.ValidateNew()
	if err != nil {
		return Answer{}, apperror.InvalidData("new answer data is invalid: "+err.Error(), err)
	}

	s.listCache.Flush()

	a, err = s.answerStore.Save(ctx, a)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Answer{}, apperror.Internal("unable to get newly created answer", err)
		}
		return Answer{}, apperror.Internal("unable to save answer", err)
	}

	cacheKey := fmt.Sprintf("id-%d", a.ID)
	s.singleCache.Set(cacheKey, a)

	return a, nil
}

func (s *service) GetByID(ctx context.Context, id int32) (Answer, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return Answer{}, apperror.Unauthenticated("authentication required to get answer data", err)
	}

	cacheKey := fmt.Sprintf("id-%d", id)
	if cached, found := s.singleCache.Get(cacheKey); found {
		return cached.(Answer), nil
	}

	a, err := s.answerStore.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Answer{}, apperror.NotFound("answer not found", err)
		}
		return Answer{}, apperror.Internal("unable to get answer", err)
	}

	s.singleCache.Set(cacheKey, a)

	return a, nil
}

func (s *service) List(ctx context.Context, opts ListOptions) (ListData, error) {
	uid, err := auth.GetUserID(ctx)
	if err != nil {
		return ListData{}, apperror.Unauthenticated("authentication required to list answers", err)
	}

	if uid != auth.AdminUserID {
		opts.Filter.ParticipantID = uid
	}

	err = opts.Validate()
	if err != nil {
		return ListData{}, apperror.OutOfRange("invalid list options: "+err.Error(), err)
	}

	cacheKey := fmt.Sprintf("list-%v", opts)
	if cached, found := s.listCache.Get(cacheKey); found {
		return cached.(ListData), nil
	}

	var offset, limit int32
	if opts.Size == -1 {
		offset = 0
		limit = 0
	} else {
		offset = opts.Size * (opts.Page - 1)
		limit = opts.Size
	}

	aa, err := s.answerStore.List(ctx, offset, limit, opts.Filter)
	if err != nil {
		return ListData{}, apperror.Internal("unable to list answers", err)
	}

	if len(aa) == 0 {
		aa = make([]Answer, 0)
	}

	totalSize, err := s.answerStore.GetTotalSize(ctx, opts.Filter)
	if err != nil {
		return ListData{}, apperror.Internal("unable to get total size while getting answer list", err)
	}

	listData := ListData{
		Answers:   aa,
		TotalSize: totalSize,
	}

	s.listCache.Set(cacheKey, listData)

	return listData, nil
}

func (s *service) Update(ctx context.Context, a Answer) (Answer, error) {
	uid, err := auth.GetUserID(ctx)
	if err != nil {
		return Answer{}, apperror.Unauthenticated("admin access is required to update answer", err)
	}

	var forceUpdate bool
	if uid == auth.AdminUserID {
		forceUpdate = true
	} else {
		forceUpdate = false
		a.ParticipantID = uid
	}

	err = a.Validate()
	if err != nil {
		return Answer{}, apperror.InvalidData("answer data is invalid: "+err.Error(), err)
	}

	s.listCache.Flush()

	a, err = s.answerStore.Update(ctx, a, forceUpdate)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("answer with id = %d does not exist", a.ID)
			return Answer{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to update answer with id = %d", a.ID)
		return Answer{}, apperror.Internal(msg, err)
	}

	cacheKey := fmt.Sprintf("id-%d", a.ID)
	s.singleCache.Set(cacheKey, a)

	return a, nil
}

func (s *service) Delete(ctx context.Context, id int32) error {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return apperror.Unauthorized("admin access is required to delete answer", err)
	}

	cacheKey := fmt.Sprintf("id-%d", id)
	s.singleCache.Delete(cacheKey)
	s.listCache.Flush()

	err = s.answerStore.Delete(ctx, id)
	if err != nil {
		msg := fmt.Sprintf("unable to delete answer with id = %d", id)
		return apperror.Internal(msg, err)
	}

	return nil
}

func NewService(logger *logrus.Entry, answerStore Store) (Service, error) {
	return &service{
		answerStore: answerStore,
		logger:      logger,
		singleCache: cachestore.NewNoopCache(),
		listCache:   cachestore.NewNoopCache(),
	}, nil
}
