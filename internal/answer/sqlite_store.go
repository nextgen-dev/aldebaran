// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package answer

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"zombiezen.com/go/sqlite"
	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/datastore"
	"gitlab.com/testycool/server/internal/format"
)

type sqliteStore struct {
	name   string
	logger *logrus.Entry
	dbPool *sqlitex.Pool
}

func (s *sqliteStore) Save(ctx context.Context, a Answer) (Answer, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into answers (participant_id, question_id)" +
		" values ($participant_id, $question_id)" +
		" returning id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Answer{}, datastore.SaveError(s.name, "unable to prepare statement", err)
	}

	stmt.SetInt64("$participant_id", int64(a.ParticipantID))
	stmt.SetInt64("$question_id", int64(a.QuestionID))

	hasRow, err := stmt.Step()
	if err != nil {
		s.resetAndClearStmt(stmt)
		return Answer{}, datastore.SaveError(s.name, "unable to execute statement", err)
	}
	if !hasRow {
		s.resetAndClearStmt(stmt)
		return Answer{}, datastore.SaveError(s.name, "unable to get saved data", datastore.ErrNoRows)
	}

	a.ID = stmt.ColumnInt32(0)

	s.resetAndClearStmt(stmt)

	switch a.Content.(type) {
	case ContentChoiceID:
		return s.saveChoiceAnswer(ctx, a)
	case ContentEssay:
		return s.saveEssayAnswer(ctx, a)
	default:
		return Answer{}, fmt.Errorf("unknown answer content type: %T", a.Content)
	}
}

func (s *sqliteStore) saveChoiceAnswer(ctx context.Context, a Answer) (Answer, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into choice_answers (answer_id, choice_id)" +
		" values ($answer_id, $choice_id)" +
		" returning (select choices.is_correct from choices where choices.id = $choice_id limit 1) as is_correct;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Answer{}, datastore.SaveError(s.name, "unable to prepare statement", err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$answer_id", int64(a.ID))
	stmt.SetInt64("$choice_id", int64(a.Content.(ContentChoiceID)))

	hasRow, err := stmt.Step()
	if err != nil {
		return Answer{}, datastore.SaveError(s.name, "unable to execute statement", err)
	}
	if !hasRow {
		return Answer{}, datastore.SaveError(s.name, "unable to get saved data", datastore.ErrNoRows)
	}

	a.Correct = stmt.ColumnInt(0) == 1

	return a, nil
}

func (s *sqliteStore) saveEssayAnswer(ctx context.Context, a Answer) (Answer, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into essay_answers (answer_id, content_format, content)" +
		" values ($answer_id, $content_format, $content)" +
		" returning is_correct;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Answer{}, datastore.SaveError(s.name, "unable to prepare statement", err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$answer_id", int64(a.ID))
	stmt.SetInt64("$content_format", int64(a.Content.(ContentEssay).ContentFormat))
	stmt.SetText("$content", a.Content.(ContentEssay).Content)

	hasRow, err := stmt.Step()
	if err != nil {
		return Answer{}, datastore.SaveError(s.name, "unable to execute statement", err)
	}
	if !hasRow {
		return Answer{}, datastore.SaveError(s.name, "unable to get saved data", datastore.ErrNoRows)
	}

	a.Correct = stmt.ColumnInt(0) == 1

	return a, nil
}

func (s *sqliteStore) GetByID(ctx context.Context, id int32) (Answer, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	query := "select answers.id as id, answers.participant_id as participant_id, answers.question_id as question_id," +
		" choice_answers.choice_id as choice_id, choices.is_correct as choice_is_correct," +
		" essay_answers.content_format as essay_content_format, essay_answers.content as essay_content, essay_answers.is_correct as essay_is_correct" +
		" from answers" +
		" left join choice_answers on answers.id = choice_answers.answer_id" +
		" left join choices on choice_answers.choice_id = choices.id" +
		" left join essay_answers on answers.id = essay_answers.answer_id" +
		" where answers.id = $id limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Answer{}, datastore.GetByIDError(s.name, "unable to prepare statement", id, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$id", int64(id))

	hasRow, err := stmt.Step()
	if err != nil {
		return Answer{}, datastore.GetByIDError(s.name, "unable to execute statement", id, err)
	}
	if !hasRow {
		return Answer{}, datastore.GetByIDError(s.name, "choice answer not found", id, datastore.ErrNoRows)
	}

	return s.answerFromStmt(stmt), nil
}

func (s *sqliteStore) List(ctx context.Context, offset int32, limit int32, f Filter) ([]Answer, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	stmt, err := s.buildListStmt(conn, offset, limit, f)
	if err != nil {
		return nil, err
	}
	defer s.resetAndClearStmt(stmt)

	var aa []Answer
	var hasRow bool
	for {
		hasRow, err = stmt.Step()
		if err != nil || !hasRow {
			break
		}
		aa = append(aa, s.answerFromStmt(stmt))
	}
	if err != nil {
		return nil, datastore.ListError(s.name, "unable to execute statement", offset, limit, f, err)
	}

	return aa, nil
}

func (s *sqliteStore) GetTotalSize(ctx context.Context, f Filter) (int32, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	stmt, err := s.buildTotalSizeStmt(conn, f)
	if err != nil {
		return 0, err
	}
	defer s.resetAndClearStmt(stmt)

	hasRow, err := stmt.Step()
	if err != nil {
		return 0, datastore.TotalSizeError(s.name, "unable to execute statement", f, err)
	}
	if !hasRow {
		return 0, datastore.TotalSizeError(s.name, "unable to get total size", f, datastore.ErrNoRows)
	}

	totalSize := stmt.GetInt64("total_size")

	return int32(totalSize), nil
}

func (s *sqliteStore) Update(ctx context.Context, a Answer, forceCorrect bool) (Answer, error) {
	switch a.Content.(type) {
	case ContentChoiceID:
		return s.updateChoiceAnswer(ctx, a)
	case ContentEssay:
		return s.updateEssayAnswer(ctx, a, forceCorrect)
	default:
		return Answer{}, fmt.Errorf("unknown answer content type: %T", a.Content)
	}
}

func (s *sqliteStore) updateChoiceAnswer(ctx context.Context, a Answer) (Answer, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "update choice_answers set" +
		" choice_id = $choice_id" +
		" where answer_id = $answer_id" +
		" returning (select choices.is_correct from choices where choices.id = $choice_id limit 1) as is_correct;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Answer{}, datastore.UpdateError(s.name, "unable to prepare statement", a.ID, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$choice_id", int64(a.Content.(ContentChoiceID)))
	stmt.SetInt64("$answer_id", int64(a.ID))

	hasRow, err := stmt.Step()
	if err != nil {
		return Answer{}, datastore.UpdateError(s.name, "unable to execute statement", a.ID, err)
	}
	if !hasRow {
		return Answer{}, datastore.UpdateError(s.name, "choice answer not found", a.ID, datastore.ErrNoRows)
	}

	a.Correct = stmt.ColumnInt(0) == 1

	return a, nil
}

func (s *sqliteStore) updateEssayAnswer(ctx context.Context, a Answer, forceCorrect bool) (Answer, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	query := "update essay_answers set"
	if forceCorrect {
		query += " is_correct = $is_correct,"
	}
	query += " content_format = $content_format, content = $content," +
		" where answer_id = $id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Answer{}, datastore.UpdateError(s.name, "unable to prepare statement", a.ID, err)
	}
	defer s.resetAndClearStmt(stmt)

	if forceCorrect {
		var isCorrectInt int64 = 0
		if a.Correct {
			isCorrectInt = 1
		}
		stmt.SetInt64("$is_correct", isCorrectInt)
	}
	stmt.SetInt64("$answer_id", int64(a.ID))
	stmt.SetInt64("$content_format", int64(a.Content.(ContentEssay).ContentFormat))
	stmt.SetText("$content", a.Content.(ContentEssay).Content)

	hasRow, err := stmt.Step()
	if err != nil {
		return Answer{}, datastore.UpdateError(s.name, "unable to execute statement", a.ID, err)
	}
	if !hasRow {
		return Answer{}, datastore.UpdateError(s.name, "choice answer not found", a.ID, datastore.ErrNoRows)
	}

	return a, nil
}

func (s *sqliteStore) Delete(ctx context.Context, id int32) error {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "delete from answers where id = $id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return datastore.DeleteError(s.name, "unable to prepare statement", id, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$id", int64(id))

	_, err = stmt.Step()
	if err != nil {
		return datastore.DeleteError(s.name, "unable to execute statement", id, err)
	}

	return nil
}

func (s *sqliteStore) resetAndClearStmt(stmt *sqlite.Stmt) {
	err := stmt.Reset()
	if err != nil {
		s.logger.WithError(err).Error("unable to reset statement")
	}

	err = stmt.ClearBindings()
	if err != nil {
		s.logger.WithError(err).Error("unable to clear statement bindings")
	}
}

func (s *sqliteStore) answerFromStmt(stmt *sqlite.Stmt) Answer {
	a := Answer{
		ID:            int32(stmt.GetInt64("id")),
		ParticipantID: int32(stmt.GetInt64("participant_id")),
		QuestionID:    int32(stmt.GetInt64("question_id")),
	}

	if choiceID := stmt.GetInt64("choice_id"); choiceID != 0 {
		a.Correct = stmt.GetInt64("choice_is_correct") == 1
		a.Content = ContentChoiceID(choiceID)
	} else if essayFormat := stmt.GetInt64("essay_content_format"); essayFormat != 0 {
		a.Correct = stmt.GetInt64("essay_is_correct") == 1
		a.Content = ContentEssay{
			ContentFormat: format.Text(essayFormat),
			Content:       stmt.GetText("essay_content"),
		}
	}

	return a
}

func (s *sqliteStore) buildListStmt(conn *sqlite.Conn, offset int32, limit int32, f Filter) (*sqlite.Stmt, error) {
	query := "select answers.id as id, answers.participant_id as participant_id, answers.question_id as question_id," +
		" choice_answers.choice_id as choice_id, choices.is_correct as choice_is_correct," +
		" essay_answers.content_format as essay_content_format, essay_answers.content as essay_content, essay_answers.is_correct as essay_is_correct" +
		" from answers" +
		" left join choice_answers on answers.id = choice_answers.answer_id" +
		" left join questions on answers.question_id = questions.id" +
		" left join choices on choice_answers.choice_id = choices.id" +
		" left join essay_answers on answers.id = essay_answers.answer_id"

	if f.ParticipantID != 0 {
		query += " where answers.participant_id = $participant_id"
	}
	if f.ExamID != 0 {
		if f.ParticipantID != 0 {
			query += " and"
		} else {
			query += " where"
		}
		query += " questions.exam_id = $exam_id"
	}
	if limit != 0 {
		query += " limit $limit"
	}
	if offset != 0 {
		if limit == 0 {
			query += " limit -1"
		}
		query += " offset $offset"
	}

	query += ";"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, datastore.ListError(s.name, "unable to prepare statement", offset, limit, f, err)
	}

	if f.ParticipantID != 0 {
		stmt.SetInt64("$participant_id", int64(f.ParticipantID))
	}
	if f.ExamID != 0 {
		stmt.SetInt64("$exam_id", int64(f.ExamID))
	}
	if offset != 0 {
		stmt.SetInt64("$offset", int64(offset))
	}
	if limit != 0 {
		stmt.SetInt64("$limit", int64(limit))
	}

	return stmt, nil
}

func (s *sqliteStore) buildTotalSizeStmt(conn *sqlite.Conn, f Filter) (*sqlite.Stmt, error) {
	query := "select count(*) as total_size from answers" +
		" left join questions on answers.question_id = questions.id"

	if f.ParticipantID != 0 {
		query += " where answers.participant_id = $participant_id"
	}
	if f.ExamID != 0 {
		if f.ParticipantID != 0 {
			query += " and"
		} else {

			query += " where"
		}
		query += " questions.exam_id = $exam_id"
	}

	query += ";"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, datastore.TotalSizeError(s.name, "unable to prepare statement", f, err)
	}

	if f.ParticipantID != 0 {
		stmt.SetInt64("$participant_id", int64(f.ParticipantID))
	}
	if f.ExamID != 0 {
		stmt.SetInt64("$exam_id", int64(f.ExamID))
	}

	return stmt, nil
}

func NewSQLiteStore(logger *logrus.Logger, dbPool *sqlitex.Pool) (Store, error) {
	const name = "answer sqlite"
	return &sqliteStore{
		name:   name,
		logger: logger.WithField("store", name),
		dbPool: dbPool,
	}, nil
}
