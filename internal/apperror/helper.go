// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package apperror

import (
	"net/http"

	"google.golang.org/grpc/codes"
)

// Unauthenticated returns an error with a human-readable message and the corresponding transport codes.
// For HTTP, the status code is 401 Unauthorized. For gRPC, the status code is 16 Unauthenticated.
func Unauthenticated(message string, err error) *Error {
	return New(message, err, http.StatusUnauthorized, codes.Unauthenticated)
}

// Unauthorized returns an error with a human-readable message and the corresponding transport codes.
// For HTTP, the status code is 403 Forbidden. For gRPC, the status code is 7 PermissionDenied.
func Unauthorized(message string, err error) *Error {
	return New(message, err, http.StatusForbidden, codes.PermissionDenied)
}

// InvalidData returns an error with a human-readable message and the corresponding transport codes.
// For HTTP, the status code is 422 UnprocessableEntity. For gRPC, the status code is 3 InvalidArgument.
func InvalidData(message string, err error) *Error {
	return New(message, err, http.StatusUnprocessableEntity, codes.InvalidArgument)
}

// InvalidInput returns an error with a human-readable message and the corresponding transport codes.
// For HTTP, the status code is 400 Bad Request. For gRPC, the status code is 3 InvalidArgument.
func InvalidInput(message string, err error) *Error {
	return New(message, err, http.StatusBadRequest, codes.InvalidArgument)
}

// Duplicate returns an error with a human-readable message and the corresponding transport codes.
// For HTTP, the status code is 409 Conflict. For gRPC, the status code is 6 AlreadyExists.
func Duplicate(message string, err error) *Error {
	return New(message, err, http.StatusConflict, codes.AlreadyExists)
}

// NotFound returns an error with a human-readable message and the corresponding transport codes.
// For HTTP, the status code is 404 NotFound. For gRPC, the status code is 5 NotFound.
func NotFound(message string, err error) *Error {
	return New(message, err, http.StatusNotFound, codes.NotFound)
}

// OutOfRange returns an error with a human-readable message and the corresponding transport codes.
// For HTTP, the status code is 404 NotFound. For gRPC, the status code is 11 OutOfRange.
func OutOfRange(message string, err error) *Error {
	return New(message, err, http.StatusNotFound, codes.OutOfRange)
}

// Internal returns an error with a human-readable message and the corresponding transport codes.
// For HTTP, the status code is 500 InternalServerError. For gRPC, the status code is 13 Internal.
func Internal(message string, err error) *Error {
	return New(message, err, http.StatusInternalServerError, codes.Internal)
}
