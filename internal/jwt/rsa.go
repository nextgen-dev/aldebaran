// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package jwt

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"

	"github.com/sirupsen/logrus"
)

func GetRSAKey(logger *logrus.Logger, rsaPrivateKeyPath string, rsaPublicKeyPath string) (*rsa.PrivateKey, error) {
	if rsaPrivateKeyPath == "" {
		logger.Warn("No RSA Key given, generating a new one")
		return GenerateRSAKey(rsaPrivateKeyPath, rsaPublicKeyPath)
	}

	priv, err := ioutil.ReadFile(rsaPrivateKeyPath)
	if err != nil {
		logger.Warn("No RSA private key found, generating a new one")
		return GenerateRSAKey(rsaPrivateKeyPath, rsaPublicKeyPath)
	}

	privPem, _ := pem.Decode(priv)
	if privPem.Type != "RSA PRIVATE KEY" {
		logger.WithField("pem_type", privPem.Type).Warn("RSA private key is of the wrong type")
	}
	privPemBytes := privPem.Bytes

	privateKey, err := x509.ParsePKCS1PrivateKey(privPemBytes)
	if err != nil {
		logger.Errorf("unable to parse RSA private key, generating a a new keypair: %v", err)
		return GenerateRSAKey(rsaPrivateKeyPath, rsaPublicKeyPath)
	}

	pub, err := ioutil.ReadFile(rsaPublicKeyPath)
	if err != nil {
		logger.Warn("No RSA public key found, generating a new one")
		return GenerateRSAKey(rsaPrivateKeyPath, rsaPublicKeyPath)
	}
	pubPem, _ := pem.Decode(pub)
	if pubPem == nil {
		logger.Warn("rsa public key not in pem format, generating a new keypair")
		return GenerateRSAKey(rsaPrivateKeyPath, rsaPublicKeyPath)
	}
	if pubPem.Type != "RSA PUBLIC KEY" {
		logger.WithField("pem_type", pubPem.Type).Warn("RSA public key is of the wrong type")
		return GenerateRSAKey(rsaPrivateKeyPath, rsaPublicKeyPath)
	}

	publicKey, err := x509.ParsePKCS1PublicKey(pubPem.Bytes)
	if err != nil {
		logger.Errorf("Unable to parse RSA public key, generating a a new keypair: %v", err)
		return GenerateRSAKey(rsaPrivateKeyPath, rsaPublicKeyPath)
	}

	if !publicKey.Equal(privateKey.Public()) {
		logger.Error("mismatch between private and public keys, generating a a new keypair")
		return GenerateRSAKey(rsaPrivateKeyPath, rsaPublicKeyPath)
	}

	return privateKey, nil
}

func GenerateRSAKey(rsaPrivateKeyPath string, rsaPublicKeyPath string) (*rsa.PrivateKey, error) {
	bitSize := 2048

	// Generate RSA key.
	key, err := rsa.GenerateKey(rand.Reader, bitSize)
	if err != nil {
		return nil, fmt.Errorf("unable to generate new RSA key: %w", err)
	}

	// Extract public component.
	pub := &key.PublicKey

	// Encode private key to PKCS#1 ASN.1 PEM.
	keyPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(key),
		},
	)

	// Encode public key to PKCS#1 ASN.1 PEM.
	pubPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: x509.MarshalPKCS1PublicKey(pub),
		},
	)

	// Write private key to file.
	if err := ioutil.WriteFile(rsaPrivateKeyPath, keyPEM, 0o600); err != nil {
		return nil, fmt.Errorf("unable to generate new RSA key: %w", err)
	}

	// Write public key to file.
	if err := ioutil.WriteFile(rsaPublicKeyPath, pubPEM, 0o600); err != nil {
		return nil, fmt.Errorf("unable to generate new RSA key: %w", err)
	}

	return key, nil
}
