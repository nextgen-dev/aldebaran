// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package jwt

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/hex"
	"time"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwt"

	"gitlab.com/testycool/server/internal/apperror"
)

const (
	UserIDKey  = "user_id"
	ExamIDKey  = "exam_id"
	IsAdminKey = "is_admin"
)

type Claims struct {
	ID        string
	IssuedAt  time.Time
	ExpiredAt time.Time
	UserID    int32
	ExamID    int32
	IsAdmin   bool
}

//go:generate mockgen --build_flags=--mod=mod -package=mock_jwt -destination=../mock/jwt/service.go . Service
type Service interface {
	GenerateAndSign(isAdmin bool, userID int32, examID int32) (string, error)
	VerifyAndDecode(token string) (Claims, error)
}

type service struct {
	key *rsa.PrivateKey
}

func (s *service) GenerateAndSign(isAdmin bool, userID int32, examID int32) (string, error) {
	expiresAt := time.Now().Add(24 * time.Hour).Unix()
	payload := jwt.New()
	_ = payload.Set(jwt.JwtIDKey, generateRandomID())
	_ = payload.Set(jwt.IssuedAtKey, time.Now().Unix())
	_ = payload.Set(jwt.ExpirationKey, expiresAt)
	_ = payload.Set(IsAdminKey, isAdmin)
	if isAdmin {
		_ = payload.Set(UserIDKey, -1)
		_ = payload.Set(ExamIDKey, -1)
	} else {
		_ = payload.Set(UserIDKey, userID)
		_ = payload.Set(ExamIDKey, examID)
	}

	signedToken, err := jwt.Sign(payload, jwa.RS256, s.key)
	if err != nil {
		return "", apperror.Internal("cannot sign token", err)
	}

	return string(signedToken), nil
}

func (s *service) VerifyAndDecode(token string) (Claims, error) {
	claims, err := jwt.Parse([]byte(token), jwt.WithVerify(jwa.RS256, s.key.PublicKey))
	if err != nil {
		return Claims{}, apperror.Internal("unable to parse token", err)
	}

	// parse private claims
	userIDClaim, _ := claims.Get(UserIDKey)
	examIDClaim, _ := claims.Get(ExamIDKey)
	isAdminClaim, _ := claims.Get(IsAdminKey)

	return Claims{
		ID:        claims.JwtID(),
		IssuedAt:  claims.IssuedAt(),
		ExpiredAt: claims.Expiration(),
		UserID:    int32(userIDClaim.(float64)),
		ExamID:    int32(examIDClaim.(float64)),
		IsAdmin:   isAdminClaim.(bool),
	}, nil
}

func NewService(key *rsa.PrivateKey) (Service, error) {
	return &service{
		key: key,
	}, nil
}

func generateRandomID() string {
	b := make([]byte, 16)
	if _, err := rand.Read(b); err != nil {
		return ""
	}
	return hex.EncodeToString(b)
}
