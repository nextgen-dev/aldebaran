// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package choice

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/apperror"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/cachestore"
	"gitlab.com/testycool/server/internal/datastore"
)

type Service interface {
	Save(ctx context.Context, c Choice) (Choice, error)
	GetByID(ctx context.Context, id int32) (Choice, error)
	List(ctx context.Context, opts ListOptions) (ListData, error)
	Update(ctx context.Context, c Choice) (Choice, error)
	Delete(ctx context.Context, id int32) error
}

type service struct {
	choiceStore Store
	logger      *logrus.Entry
	singleCache cachestore.Cache
	listCache   cachestore.Cache
}

func (s *service) Save(ctx context.Context, c Choice) (Choice, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Choice{}, apperror.Unauthorized("admin access is required to create new choice", err)
	}

	err = c.ValidateNew()
	if err != nil {
		return Choice{}, apperror.InvalidData("new choice data is invalid: "+err.Error(), err)
	}

	s.listCache.Flush()

	c, err = s.choiceStore.Save(ctx, c)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Choice{}, apperror.Internal("unable to get newly created choice", err)
		}
		return Choice{}, apperror.Internal("unable to save choice", err)
	}

	cacheKey := fmt.Sprintf("id-%d", c.ID)
	s.singleCache.Set(cacheKey, c)

	return c, nil
}

func (s *service) GetByID(ctx context.Context, id int32) (Choice, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return Choice{}, apperror.Unauthenticated("authentication required to get choice data", err)
	}

	cacheKey := fmt.Sprintf("id-%d", id)
	if cached, ok := s.singleCache.Get(cacheKey); ok {
		return cached.(Choice), nil
	}

	c, err := s.choiceStore.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Choice{}, apperror.NotFound("choice not found", err)
		}
		return Choice{}, apperror.Internal("unable to get choice", err)
	}

	s.singleCache.Set(cacheKey, c)

	return c, nil
}

func (s *service) List(ctx context.Context, opts ListOptions) (ListData, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return ListData{}, apperror.Unauthenticated("authentication required to list choices", err)
	}

	err = opts.Validate()
	if err != nil {
		return ListData{}, apperror.OutOfRange("invalid list options: "+err.Error(), err)
	}

	cacheKey := fmt.Sprintf("list-%v", opts)
	if cached, ok := s.listCache.Get(cacheKey); ok {
		return cached.(ListData), nil
	}

	var offset, limit int32
	if opts.Size == -1 {
		offset = 0
		limit = 0
	} else {
		offset = opts.Size * (opts.Page - 1)
		limit = opts.Size
	}

	cc, err := s.choiceStore.List(ctx, offset, limit, opts.Filter)
	if err != nil {
		return ListData{}, apperror.Internal("unable to list choices", err)
	}

	if len(cc) == 0 {
		cc = make([]Choice, 0)
	}

	totalSize, err := s.choiceStore.GetTotalSize(ctx, opts.Filter)
	if err != nil {
		return ListData{}, apperror.Internal("unable to get total size while getting choice list", err)
	}

	listData := ListData{
		Choices:   cc,
		TotalSize: totalSize,
	}

	s.listCache.Set(cacheKey, listData)

	return listData, nil
}

func (s *service) Update(ctx context.Context, c Choice) (Choice, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Choice{}, apperror.Unauthorized("admin access is required to update choice", err)
	}

	err = c.Validate()
	if err != nil {
		return Choice{}, apperror.InvalidData("choice data is invalid: "+err.Error(), err)
	}

	s.listCache.Flush()

	c, err = s.choiceStore.Update(ctx, c)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("choice with id = %d does not exist", c.ID)
			return Choice{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to update choice with id = %d", c.ID)
		return Choice{}, apperror.Internal(msg, err)
	}

	cacheKey := fmt.Sprintf("id-%d", c.ID)
	s.singleCache.Set(cacheKey, c)

	return c, nil
}

func (s *service) Delete(ctx context.Context, id int32) error {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return apperror.Unauthorized("admin access is required to delete choice", err)
	}

	cacheKey := fmt.Sprintf("id-%d", id)
	s.singleCache.Delete(cacheKey)
	s.listCache.Flush()

	err = s.choiceStore.Delete(ctx, id)
	if err != nil {
		msg := fmt.Sprintf("unable to delete choice with id = %d", id)
		return apperror.Internal(msg, err)
	}

	return nil
}

func NewService(logger *logrus.Entry, choiceStore Store) (Service, error) {
	return &service{
		choiceStore: choiceStore,
		logger:      logger,
		singleCache: cachestore.NewMemoryCache(),
		listCache:   cachestore.NewMemoryCache(),
	}, nil
}
