// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package choice

import (
	"errors"
	"fmt"

	"gitlab.com/testycool/server/internal/format"
)

var (
	ErrInvalidID            = errors.New("invalid choice id")
	ErrInvalidContentFormat = errors.New("invalid choice content format")
	ErrInvalidContent       = errors.New("invalid choice content")
	ErrInvalidQuestionID    = errors.New("invalid choice question id")
)

type Choice struct {
	ID            int32       `json:"id,omitempty"`
	Correct       bool        `json:"correct,omitempty"`
	ContentFormat format.Text `json:"content_format,omitempty"`
	Content       string      `json:"content,omitempty"`
	QuestionID    int32       `json:"question_id,omitempty"`
}

func (c *Choice) Validate() error {
	if c.ID < 0 {
		return ErrInvalidID
	}

	if !c.ContentFormat.Valid() {
		return ErrInvalidContentFormat
	}

	if c.Content == "" {
		return ErrInvalidContent
	}

	if c.QuestionID < 0 {
		return ErrInvalidQuestionID
	}

	return nil
}

func (c *Choice) ValidateNew() error {
	if !c.ContentFormat.Valid() {
		return ErrInvalidContentFormat
	}

	if c.Content == "" {
		return ErrInvalidContent
	}

	if c.QuestionID < 0 {
		return ErrInvalidQuestionID
	}

	return nil
}

type Filter struct {
	QuestionID int32 `json:"question_id,omitempty"`
}

type ListOptions struct {
	Filter Filter `json:"filter,omitempty"`
	Size   int32  `json:"size,omitempty"`
	Page   int32  `json:"page,omitempty"`
}

func (o *ListOptions) Validate() error {
	if o.Size < -1 {
		return fmt.Errorf("invalid page size (%d < -1)", o.Size)
	}

	if o.Page < 1 {
		return fmt.Errorf("invalid page number (%d < 1)", o.Page)
	}

	return nil
}

type ListData struct {
	Choices   []Choice `json:"choices,omitempty"`
	TotalSize int32    `json:"total_size,omitempty"`
}
