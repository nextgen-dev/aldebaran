// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package choice

import (
	"context"

	"github.com/sirupsen/logrus"
	"zombiezen.com/go/sqlite"
	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/datastore"
	"gitlab.com/testycool/server/internal/format"
)

type sqliteStore struct {
	name   string
	logger *logrus.Entry
	dbPool *sqlitex.Pool
}

func (s *sqliteStore) Save(ctx context.Context, c Choice) (Choice, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into choices (is_correct, content_format, content, question_id)" +
		" values ($is_correct, $content_format, $content, $question_id)" +
		" returning id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Choice{}, datastore.SaveError(s.name, "unable to prepare statement", err)
	}
	defer s.resetAndClearStmt(stmt)

	// go doesn't have a way to cast a bool to an int, so we have to do it manually
	var isCorrectInt int64 = 0
	if c.Correct {
		isCorrectInt = 1
	}

	stmt.SetInt64("$is_correct", isCorrectInt)
	stmt.SetInt64("$content_format", int64(c.ContentFormat))
	stmt.SetText("$content", c.Content)
	stmt.SetInt64("$question_id", int64(c.QuestionID))

	hasRow, err := stmt.Step()
	if err != nil {
		return Choice{}, datastore.SaveError(s.name, "unable to execute statement", err)
	}
	if !hasRow {
		return Choice{}, datastore.SaveError(s.name, "unable to get saved data", datastore.ErrNoRows)
	}

	c.ID = int32(stmt.ColumnInt32(0))

	return c, nil
}

func (s *sqliteStore) GetByID(ctx context.Context, id int32) (Choice, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, is_correct, content_format, content, question_id from choices where id = $id limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Choice{}, datastore.GetByIDError(s.name, "unable to prepare statement", id, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$id", int64(id))

	hasRow, err := stmt.Step()
	if err != nil {
		return Choice{}, datastore.GetByIDError(s.name, "unable to execute statement", id, err)
	}
	if !hasRow {
		return Choice{}, datastore.GetByIDError(s.name, "choice not found", id, datastore.ErrNoRows)
	}

	return s.choiceFromStmt(stmt), nil
}

func (s *sqliteStore) List(ctx context.Context, offset int32, limit int32, f Filter) ([]Choice, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	stmt, err := s.buildListStmt(conn, offset, limit, f)
	if err != nil {
		return nil, err
	}
	defer s.resetAndClearStmt(stmt)

	var cc []Choice
	var hasRow bool
	for {
		hasRow, err = stmt.Step()
		if err != nil || !hasRow {
			break
		}
		cc = append(cc, s.choiceFromStmt(stmt))
	}
	if err != nil {
		return nil, datastore.ListError(s.name, "unable to execute statement", offset, limit, f, err)
	}

	return cc, nil
}

func (s *sqliteStore) GetTotalSize(ctx context.Context, f Filter) (int32, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select count(*) as total_size from choices;"

	stmt, err := s.buildTotalSizeStmt(conn, f)
	if err != nil {
		return 0, err
	}
	defer s.resetAndClearStmt(stmt)

	hasRow, err := stmt.Step()
	if err != nil {
		return 0, datastore.TotalSizeError(s.name, "unable to execute statement", f, err)
	}
	if !hasRow {
		return 0, datastore.TotalSizeError(s.name, "unable to get total size", f, datastore.ErrNoRows)
	}

	totalSize := stmt.GetInt64("total_size")

	return int32(totalSize), nil
}

func (s *sqliteStore) Update(ctx context.Context, c Choice) (Choice, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "update choices set" +
		" is_correct = $is_correct, content_format = $content_format, content = $content, question_id = $question_id" +
		" where id = $id" +
		" returning id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Choice{}, datastore.UpdateError(s.name, "unable to prepare statement", c.ID, err)
	}
	defer s.resetAndClearStmt(stmt)

	// go doesn't have a way to cast a bool to an int, so we have to do it manually
	var isCorrectInt int64 = 0
	if c.Correct {
		isCorrectInt = 1
	}

	stmt.SetInt64("$is_correct", isCorrectInt)
	stmt.SetInt64("$content_format", int64(c.ContentFormat))
	stmt.SetText("$content", c.Content)
	stmt.SetInt64("$question_id", int64(c.QuestionID))
	stmt.SetInt64("$id", int64(c.ID))

	hasRow, err := stmt.Step()
	if err != nil {
		return Choice{}, datastore.UpdateError(s.name, "unable to execute statement", c.ID, err)
	}
	if !hasRow {
		return Choice{}, datastore.UpdateError(s.name, "choice not found", c.ID, datastore.ErrNoRows)
	}

	return c, nil
}

func (s *sqliteStore) Delete(ctx context.Context, id int32) error {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "delete from choices where id = $id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return datastore.DeleteError(s.name, "unable to prepare statement", id, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$id", int64(id))

	_, err = stmt.Step()
	if err != nil {
		return datastore.DeleteError(s.name, "unable to execute statement", id, err)
	}

	return nil
}

func (s *sqliteStore) resetAndClearStmt(stmt *sqlite.Stmt) {
	err := stmt.Reset()
	if err != nil {
		s.logger.WithError(err).Error("unable to reset statement")
	}

	err = stmt.ClearBindings()
	if err != nil {
		s.logger.WithError(err).Error("unable to clear statement bindings")
	}
}

func (s *sqliteStore) choiceFromStmt(stmt *sqlite.Stmt) Choice {
	return Choice{
		ID:            int32(stmt.GetInt64("id")),
		Correct:       stmt.GetInt64("is_correct") == 1,
		ContentFormat: format.Text(stmt.GetInt64("content_format")),
		Content:       stmt.GetText("content"),
		QuestionID:    int32(stmt.GetInt64("question_id")),
	}
}

func (s *sqliteStore) buildListStmt(conn *sqlite.Conn, offset int32, limit int32, f Filter) (*sqlite.Stmt, error) {
	query := "select id, is_correct, content_format, content, question_id from choices"

	if f.QuestionID != 0 {
		query += " where question_id = $question_id"
	}
	if limit != 0 {
		query += " limit $limit"
	}
	if offset != 0 {
		if limit == 0 {
			query += " limit -1"
		}
		query += " offset $offset"
	}

	query += ";"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, datastore.ListError(s.name, "unable to prepare statement", offset, limit, f, err)
	}

	if f.QuestionID != 0 {
		stmt.SetInt64("$question_id", int64(f.QuestionID))
	}
	if offset != 0 {
		stmt.SetInt64("$offset", int64(offset))
	}
	if limit != 0 {
		stmt.SetInt64("$limit", int64(limit))
	}

	return stmt, nil
}

func (s *sqliteStore) buildTotalSizeStmt(conn *sqlite.Conn, f Filter) (*sqlite.Stmt, error) {
	query := "select count(*) as total_size from choices"

	if f.QuestionID != 0 {
		query += " where question_id = $question_id"
	}

	query += ";"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, datastore.TotalSizeError(s.name, "unable to prepare statement", f, err)
	}

	if f.QuestionID != 0 {
		stmt.SetInt64("$question_id", int64(f.QuestionID))
	}

	return stmt, nil
}

func NewSQLiteStore(logger *logrus.Logger, dbPool *sqlitex.Pool) (Store, error) {
	const name = "choice sqlite"
	return &sqliteStore{
		name:   name,
		logger: logger.WithField("store", name),
		dbPool: dbPool,
	}, nil
}
