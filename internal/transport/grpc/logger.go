// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package grpc

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
)

func (t *transport) configureRequestLogger() error {
	if t.requestLogger != nil && t.requestLogFile != nil {
		return nil
	}

	logger := logrus.New()
	logger.SetLevel(logrus.InfoLevel)

	if t.config.Verbose {
		logger.SetOutput(os.Stdout)
		logger.SetFormatter(&logrus.TextFormatter{
			ForceQuote:    true,
			FullTimestamp: true,
			PadLevelText:  true,
		})
	} else {
		logger.SetOutput(ioutil.Discard)
	}

	logFileName := filepath.Join(t.config.Runtime.LogDir, "grpc.log")
	logFile, err := os.OpenFile(logFileName, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0644)
	if err != nil {
		return fmt.Errorf("unable to create grpc request log file: %w", err)
	}

	logger.AddHook(lfshook.NewHook(
		logFile,
		&logrus.TextFormatter{
			DisableColors: true,
			ForceQuote:    true,
			FullTimestamp: true,
			PadLevelText:  false,
		},
	))

	t.requestLogger = logger
	t.requestLogFile = logFile
	return nil
}
