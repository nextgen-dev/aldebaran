// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package grpc

import (
	"fmt"

	"google.golang.org/grpc/reflection"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/transport/grpc/server"
)

func (t *transport) registerServices() error {
	if err := t.services.Validate(); err != nil {
		return fmt.Errorf("invalid service container: %w", err)
	}

	// instantiate service server implementations
	const serviceLogFieldKey = "service_server"
	authServer := server.NewAuthServer(t.logger.WithField(serviceLogFieldKey, "auth"), t.services.AuthService)
	examServer := server.NewExamServer(t.logger.WithField(serviceLogFieldKey, "exam"), t.services.ExamService)
	participantServer := server.NewParticipantServer(
		t.logger.WithField(serviceLogFieldKey, "participant"),
		t.services.ParticipantService,
	)
	questionServer := server.NewQuestionServer(t.logger.WithField(serviceLogFieldKey, "question"), t.services.QuestionService)
	choiceServer := server.NewChoiceServer(t.logger.WithField(serviceLogFieldKey, "choice"), t.services.ChoiceService)
	answerServer := server.NewAnswerServer(t.logger.WithField(serviceLogFieldKey, "answer"), t.services.AnswerService)
	attemptServer := server.NewAttemptServer(
		t.logger.WithField(serviceLogFieldKey, "attempt"), 
		t.services.AttemptService, 
		t.services.QuestionService, 
		t.services.AnswerService)
	analyticsServer := server.NewAnalyticsServer(t.logger.WithField(serviceLogFieldKey, "analytics"), t.services.AnalyticsService)

	// register service server implementations
	t.grpcServer.RegisterService(&pb.AuthService_ServiceDesc, authServer)
	t.grpcServer.RegisterService(&pb.ExamService_ServiceDesc, examServer)
	t.grpcServer.RegisterService(&pb.ParticipantService_ServiceDesc, participantServer)
	t.grpcServer.RegisterService(&pb.QuestionService_ServiceDesc, questionServer)
	t.grpcServer.RegisterService(&pb.ChoiceService_ServiceDesc, choiceServer)
	t.grpcServer.RegisterService(&pb.AnswerService_ServiceDesc, answerServer)
	t.grpcServer.RegisterService(&pb.AttemptService_ServiceDesc, attemptServer)
	t.grpcServer.RegisterService(&pb.AnalyticsService_ServiceDesc, analyticsServer)

	// register grpc reflection if enabled
	if t.config.GRPC.EnableReflection {
		t.logger.Debug("gRPC reflection registered")
		reflection.Register(t.grpcServer)
	}

	return nil
}
