// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/participant"
)

type participantServer struct {
	pb.UnimplementedParticipantServiceServer
	logger             *logrus.Entry
	participantService participant.Service
}

func (s *participantServer) CreateParticipant(
	ctx context.Context,
	request *pb.CreateParticipantRequest,
) (*pb.CreateParticipantResponse, error) {
	p := participant.Participant{
		Code:   request.GetCode(),
		Name:   request.GetName(),
		ExamID: request.GetExamId(),
	}

	p, err := s.participantService.Save(ctx, p)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.CreateParticipantResponse{
		Participant: s.participantToProto(p),
	}, nil
}

func (s *participantServer) GetParticipant(
	ctx context.Context,
	request *pb.GetParticipantRequest,
) (*pb.GetParticipantResponse, error) {
	var p participant.Participant
	var err error
	if id := request.GetId(); id != 0 {
		p, err = s.participantService.GetByID(ctx, id)
	} else {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetParticipantResponse{
		Participant: s.participantToProto(p),
	}, nil
}

func (s *participantServer) ListParticipants(
	ctx context.Context,
	request *pb.ListParticipantsRequest,
) (*pb.ListParticipantsResponse, error) {
	size := request.GetSize()
	page := request.GetPage()

	filter := participant.ListFilter{
		ExamID: request.Filter.ExamId,
	}

	pp, totalSize, err := s.participantService.List(ctx, size, page, filter)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	var participants []*pb.Participant
	for i := range pp {
		participants = append(participants, s.participantToProto(pp[i]))
	}

	currentPageSize := int32(len(participants))

	// if the actual requested size exceed total size (e.g. list all resources), set current page to 1
	if currentPageSize*page > totalSize {
		page = 1
	}

	return &pb.ListParticipantsResponse{
		Participants: participants,
		TotalSize:    totalSize,
		Page:         page,
		Size:         currentPageSize,
	}, nil
}

func (s *participantServer) UpdateParticipant(
	ctx context.Context,
	request *pb.UpdateParticipantRequest,
) (*pb.UpdateParticipantResponse, error) {
	reqP := request.GetParticipant()

	if reqP == nil {
		return nil, status.Error(codes.InvalidArgument, "participant data is missing")
	}
	if reqP.GetId() == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must not be zero")
	}

	p := participant.Participant{
		ID:     reqP.GetId(),
		Code:   reqP.GetCode(),
		Name:   reqP.GetName(),
		ExamID: reqP.GetExamId(),
	}

	p, err := s.participantService.Update(ctx, p)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.UpdateParticipantResponse{
		Participant: s.participantToProto(p),
	}, nil
}

func (s *participantServer) DeleteParticipant(
	ctx context.Context,
	request *pb.DeleteParticipantRequest,
) (*emptypb.Empty, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must not be zero")
	}

	err := s.participantService.Delete(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &emptypb.Empty{}, nil
}

func (*participantServer) participantToProto(p participant.Participant) *pb.Participant {
	return &pb.Participant{
		Id:     p.ID,
		Code:   p.Code,
		Name:   p.Name,
		ExamId: p.ExamID,
	}
}

func NewParticipantServer(logger *logrus.Entry, participantService participant.Service) pb.ParticipantServiceServer {
	return &participantServer{
		logger:             logger,
		participantService: participantService,
	}
}
