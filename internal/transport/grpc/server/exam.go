// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/exam"
)

type examServer struct {
	pb.UnimplementedExamServiceServer
	logger      *logrus.Entry
	examService exam.Service
}

func (s *examServer) CreateExam(ctx context.Context, request *pb.CreateExamRequest) (*pb.CreateExamResponse, error) {
	e := exam.Exam{
		Password:  request.GetPassword(),
		Title:     request.GetTitle(),
		TimeLimit: time.Duration(request.GetTimeLimit()) * time.Second,
		StartAt:   request.GetStartAt().AsTime(),
	}

	e, err := s.examService.Save(ctx, e)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.CreateExamResponse{
		Exam: s.examToProto(e),
	}, nil
}

func (s *examServer) GetExam(ctx context.Context, request *pb.GetExamRequest) (*pb.GetExamResponse, error) {
	var e exam.Exam
	var err error
	if id := request.GetId(); id != 0 {
		e, err = s.examService.GetByID(ctx, id)
	} else if password := request.GetPassword(); password != "" {
		e, err = s.examService.GetByPassword(ctx, password)
	} else {
		return nil, status.Error(codes.InvalidArgument, "either id or password must be provided")
	}

	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetExamResponse{
		Exam: s.examToProto(e),
	}, nil
}

func (s *examServer) ListExams(ctx context.Context, request *pb.ListExamsRequest) (*pb.ListExamsResponse, error) {
	size := request.GetSize()
	page := request.GetPage()
	ee, totalSize, err := s.examService.List(ctx, size, page)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	var pbExams []*pb.Exam
	for i := range ee {
		pbExams = append(pbExams, s.examToProto(ee[i]))
	}

	currentPageSize := int32(len(pbExams))

	// if the actual requested size exceed total size (e.g. list all resources), set current page to 1
	if currentPageSize*page > totalSize {
		page = 1
	}

	return &pb.ListExamsResponse{
		Exams:     pbExams,
		Size:      currentPageSize,
		Page:      page,
		TotalSize: totalSize,
	}, nil
}

func (s *examServer) UpdateExam(ctx context.Context, request *pb.UpdateExamRequest) (*pb.UpdateExamResponse, error) {
	reqPbExam := request.GetExam()

	if reqPbExam == nil {
		return nil, status.Error(codes.InvalidArgument, "exam is missing")
	}
	if reqPbExam.GetId() == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must not be zero")
	}

	e := exam.Exam{
		ID:        reqPbExam.GetId(),
		Password:  reqPbExam.GetPassword(),
		Title:     reqPbExam.GetTitle(),
		TimeLimit: time.Duration(reqPbExam.GetTimeLimit()) * time.Second,
		StartAt:   reqPbExam.GetStartAt().AsTime(),
	}
	switch reqPbExam.GetStatus() {
	case pb.Exam_WAITING:
		e.Status = exam.StatusWaiting
	case pb.Exam_STARTED:
		e.Status = exam.StatusStarted
	case pb.Exam_DONE:
		e.Status = exam.StatusDone
	default:
		return nil, status.Error(codes.InvalidArgument, "exam status is invalid")
	}

	e, err := s.examService.Update(ctx, e)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.UpdateExamResponse{
		Exam: s.examToProto(e),
	}, nil
}

func (s *examServer) DeleteExam(ctx context.Context, request *pb.DeleteExamRequest) (*emptypb.Empty, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must not be zero")
	}

	err := s.examService.Delete(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &emptypb.Empty{}, nil
}

func (*examServer) examToProto(e exam.Exam) *pb.Exam {
	pbExam := &pb.Exam{
		Id:        e.ID,
		Password:  e.Password,
		Title:     e.Title,
		TimeLimit: int32(e.TimeLimit.Seconds()),
		StartAt:   timestamppb.New(e.StartAt),
	}
	switch e.Status {
	case exam.StatusWaiting:
		pbExam.Status = pb.Exam_WAITING
	case exam.StatusStarted:
		pbExam.Status = pb.Exam_STARTED
	case exam.StatusDone:
		pbExam.Status = pb.Exam_DONE
	default:
		pbExam.Status = pb.Exam_UNKNOWN
	}

	return pbExam
}

func NewExamServer(logger *logrus.Entry, examService exam.Service) pb.ExamServiceServer {
	return &examServer{
		logger:      logger,
		examService: examService,
	}
}
