// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	pb "gitlab.com/testycool/server/api/protobuf/gen/go/testycool"
	"gitlab.com/testycool/server/internal/answer"
	"gitlab.com/testycool/server/internal/format"
)

type answerServer struct {
	pb.UnimplementedAnswerServiceServer
	logger        *logrus.Entry
	answerService answer.Service
}

func (s *answerServer) GetAnswer(
	ctx context.Context,
	request *pb.GetAnswerRequest,
) (*pb.GetAnswerResponse, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	a, err := s.answerService.GetByID(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.GetAnswerResponse{
		Answer: s.answerToProto(a),
	}, nil
}

func (s *answerServer) ListAnswers(
	ctx context.Context,
	request *pb.ListAnswersRequest,
) (*pb.ListAnswersResponse, error) {
	size := request.GetSize()
	page := request.GetPage()

	listOpt := answer.ListOptions{
		Size: size,
		Page: page,
	}

	if reqFilter := request.GetFilter(); reqFilter != nil {
		listOpt.Filter = answer.Filter{
			ExamID:        reqFilter.GetExamId(),
			ParticipantID: reqFilter.GetParticipantId(),
		}
	}

	listData, err := s.answerService.List(ctx, listOpt)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	var aa []*pb.Answer
	for _, a := range listData.Answers {
		aa = append(aa, s.answerToProto(a))
	}

	currentPageSize := int32(len(aa))

	// if the actual requested size exceeds total size (e.g. list all resources), set current page to 1
	if currentPageSize*page > listData.TotalSize {
		page = 1
	}

	return &pb.ListAnswersResponse{
		Answers:   aa,
		TotalSize: listData.TotalSize,
		Page:      page,
		Size:      currentPageSize,
	}, nil
}

func (s *answerServer) CreateAnswer(
	ctx context.Context,
	request *pb.CreateAnswerRequest,
) (*pb.CreateAnswerResponse, error) {
	a := answer.Answer{
		Correct:       false,
		ParticipantID: request.GetParticipantId(),
		QuestionID:    request.GetQuestionId(),
	}

	switch c := request.Content.(type) {
	case *pb.CreateAnswerRequest_ChoiceId:
		if c.ChoiceId == 0 {
			return nil, status.Error(codes.InvalidArgument, "choice id must be provided")
		}
		a.Content = answer.ContentChoiceID(c.ChoiceId)
	case *pb.CreateAnswerRequest_Essay:
		if c.Essay == nil {
			return nil, status.Error(codes.InvalidArgument, "essay data must be provided")
		}
		a.Content = answer.ContentEssay{
			ContentFormat: format.Text(c.Essay.GetFormat()),
			Content:       c.Essay.GetContent(),
		}
	default:
		return nil, status.Error(codes.InvalidArgument, "content must be provided")
	}

	a, err := s.answerService.Save(ctx, a)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.CreateAnswerResponse{
		Answer: s.answerToProto(a),
	}, nil
}

func (s *answerServer) UpdateAnswer(ctx context.Context, request *pb.UpdateAnswerRequest) (*pb.UpdateAnswerResponse, error) {
	reqA := request.GetAnswer()
	if reqA == nil {
		return nil, status.Error(codes.InvalidArgument, "answer data must be provided")
	}

	if reqA.GetId() == 0 {
		return nil, status.Error(codes.InvalidArgument, "answer id must be provided")
	}

	a := answer.Answer{
		ID:            reqA.GetId(),
		Correct:       false,
		ParticipantID: reqA.GetParticipantId(),
		QuestionID:    reqA.GetQuestionId(),
	}

	switch c := reqA.Content.(type) {
	case *pb.Answer_ChoiceId:
		if c.ChoiceId == 0 {
			return nil, status.Error(codes.InvalidArgument, "choice id must be provided")
		}
		a.Content = answer.ContentChoiceID(c.ChoiceId)
	case *pb.Answer_Essay:
		if c.Essay == nil {
			return nil, status.Error(codes.InvalidArgument, "essay data must be provided")
		}
		a.Content = answer.ContentEssay{
			ContentFormat: format.Text(c.Essay.GetFormat()),
			Content:       c.Essay.GetContent(),
		}
	default:
		return nil, status.Error(codes.InvalidArgument, "content must be provided")
	}

	a, err := s.answerService.Update(ctx, a)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &pb.UpdateAnswerResponse{
		Answer: s.answerToProto(a),
	}, nil
}

func (s *answerServer) DeleteAnswer(ctx context.Context, request *pb.DeleteAnswerRequest) (*emptypb.Empty, error) {
	id := request.GetId()
	if id == 0 {
		return nil, status.Error(codes.InvalidArgument, "id must be provided")
	}

	err := s.answerService.Delete(ctx, id)
	if err != nil {
		return nil, errorToStatus(s.logger, err)
	}

	return &emptypb.Empty{}, nil
}

func (s *answerServer) answerToProto(a answer.Answer) *pb.Answer {
	pbAnswer := &pb.Answer{
		Id:            a.ID,
		ParticipantId: a.ParticipantID,
		QuestionId:    a.QuestionID,
		IsCorrect:     a.Correct,
	}

	switch c := a.Content.(type) {
	case answer.ContentChoiceID:
		pbAnswer.Content = &pb.Answer_ChoiceId{
			ChoiceId: int32(c),
		}
	case answer.ContentEssay:
		pbAnswer.Content = &pb.Answer_Essay{
			Essay: &pb.EssayAnswer{
				Format:  pb.TextFormat(c.ContentFormat),
				Content: c.Content,
			},
		}
	}

	return pbAnswer
}

func NewAnswerServer(logger *logrus.Entry, answerService answer.Service) pb.AnswerServiceServer {
	return &answerServer{
		logger:        logger,
		answerService: answerService,
	}
}