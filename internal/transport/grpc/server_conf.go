// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package grpc

import (
	"context"

	grpcMiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpcAuth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	grpcLogrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpcRecovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpcCtxTags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/testycool/server/internal/auth"
)

//nolint:unparam // return nil for future use case
func (t *transport) configureServer() error {
	if t.grpcServer != nil {
		return nil
	}

	unaryOpts := []grpc.UnaryServerInterceptor{
		grpcCtxTags.UnaryServerInterceptor(
			grpcCtxTags.WithFieldExtractor(grpcCtxTags.CodeGenRequestFieldExtractor),
		),
		grpcRecovery.UnaryServerInterceptor(grpcRecovery.WithRecoveryHandler(t.recoverFunc)),
		grpcAuth.UnaryServerInterceptor(t.authFunc),
	}

	streamOpts := []grpc.StreamServerInterceptor{
		grpcCtxTags.StreamServerInterceptor(
			grpcCtxTags.WithFieldExtractor(grpcCtxTags.CodeGenRequestFieldExtractor),
		),
		grpcRecovery.StreamServerInterceptor(grpcRecovery.WithRecoveryHandler(t.recoverFunc)),
		grpcAuth.StreamServerInterceptor(t.authFunc),
	}

	if t.config.GRPC.LogRequests && t.requestLogger != nil {
		logrusEntry := t.requestLogger.WithField("logger", "grpc_request")
		unaryOpts = append(unaryOpts, grpcLogrus.UnaryServerInterceptor(logrusEntry))
		streamOpts = append(streamOpts, grpcLogrus.StreamServerInterceptor(logrusEntry))
	}

	if t.config.GRPC.CertFile != "" && t.config.GRPC.KeyFile != "" {
		tlsCredentials, err := t.loadTLSCredentials()
		if err != nil {
			t.logger.Fatal("cannot load TLS credentials: ", err)
		}

		t.grpcServer = grpc.NewServer(
			grpc.Creds(tlsCredentials),
			grpcMiddleware.WithUnaryServerChain(unaryOpts...),
			grpcMiddleware.WithStreamServerChain(streamOpts...),
		)
	} else {
		t.grpcServer = grpc.NewServer(
			grpcMiddleware.WithUnaryServerChain(unaryOpts...),
			grpcMiddleware.WithStreamServerChain(streamOpts...),
		)
	}

	return nil
}

func (t *transport) authFunc(ctx context.Context) (context.Context, error) {
	token, err := grpcAuth.AuthFromMD(ctx, "bearer")
	if err != nil {
		return ctx, nil //nolint:nilerr // don't return error, check auth in service level instead
	}

	claims, err := t.services.JWTService.VerifyAndDecode(token)
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "invalid auth token")
	}

	newCtx := context.WithValue(ctx, auth.UserIDContextKey, claims.UserID)
	newCtx = context.WithValue(newCtx, auth.IsAdminContextKey, claims.IsAdmin)
	newCtx = context.WithValue(newCtx, auth.ExamIDContextKey, claims.ExamID)

	return newCtx, nil
}

func (t *transport) recoverFunc(err interface{}) error {
	t.logger.Errorf("panic recovered: %v", err)
	return status.Error(codes.Internal, "internal server error")
}
