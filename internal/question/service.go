// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package question

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/apperror"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/cachestore"
	"gitlab.com/testycool/server/internal/datastore"
)

type Service interface {
	Save(ctx context.Context, q Question) (Question, error)
	GetByID(ctx context.Context, id int32) (Question, error)
	List(ctx context.Context, opts ListOptions) (ListData, error)
	Update(ctx context.Context, q Question) (Question, error)
	Delete(ctx context.Context, id int32) error
}

type service struct {
	questionStore Store
	logger        *logrus.Entry
	singleCache   cachestore.Cache
	listCache     cachestore.Cache
}

func (s *service) Save(ctx context.Context, q Question) (Question, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Question{}, apperror.Unauthorized("admin access is required to create new question", err)
	}

	err = q.ValidateNew()
	if err != nil {
		return Question{}, apperror.InvalidData("new question data is invalid: "+err.Error(), err)
	}

	s.listCache.Flush()

	q, err = s.questionStore.Save(ctx, q)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Question{}, apperror.Internal("unable to get newly created question", err)
		}
		return Question{}, apperror.Internal("unable to save question", err)
	}

	cacheKey := fmt.Sprintf("id-%d", q.ID)
	s.singleCache.Set(cacheKey, q)

	return q, nil
}

func (s *service) GetByID(ctx context.Context, id int32) (Question, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return Question{}, apperror.Unauthenticated("authentication required to get question data", err)
	}

	cacheKey := fmt.Sprintf("id-%d", id)
	if cached, found := s.singleCache.Get(cacheKey); found {
		return cached.(Question), nil
	}

	q, err := s.questionStore.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Question{}, apperror.NotFound("question not found", err)
		}
		return Question{}, apperror.Internal("unable to get question", err)
	}

	s.singleCache.Set(cacheKey, q)

	return q, nil
}

func (s *service) List(ctx context.Context, opts ListOptions) (ListData, error) {
	err := auth.CheckAuthentication(ctx)
	if err != nil {
		return ListData{}, apperror.Unauthenticated("authentication required to list questions", err)
	}

	err = opts.Validate()
	if err != nil {
		return ListData{}, apperror.OutOfRange("invalid list options: "+err.Error(), err)
	}

	cacheKey := fmt.Sprintf("list-%v", opts)
	if cached, found := s.listCache.Get(cacheKey); found {
		return cached.(ListData), nil
	}

	var offset, limit int32
	if opts.Size == -1 {
		offset = 0
		limit = 0
	} else {
		offset = opts.Size * (opts.Page - 1)
		limit = opts.Size
	}

	qq, err := s.questionStore.List(ctx, offset, limit, opts.Filter)
	if err != nil {
		return ListData{}, apperror.Internal("unable to list questions", err)
	}

	if len(qq) == 0 {
		qq = make([]Question, 0)
	}

	totalSize, err := s.questionStore.GetTotalSize(ctx, opts.Filter)
	if err != nil {
		return ListData{}, apperror.Internal("unable to get total size while getting question list", err)
	}

	listData := ListData{
		Questions: qq,
		TotalSize: totalSize,
	}

	s.listCache.Set(cacheKey, listData)

	return listData, nil
}

func (s *service) Update(ctx context.Context, q Question) (Question, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Question{}, apperror.Unauthorized("admin access is required to update question", err)
	}

	err = q.Validate()
	if err != nil {
		return Question{}, apperror.InvalidData("question data is invalid: "+err.Error(), err)
	}

	s.listCache.Flush()

	q, err = s.questionStore.Update(ctx, q)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("question with id = %d does not exist", q.ID)
			return Question{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to update question with id = %d", q.ID)
		return Question{}, apperror.Internal(msg, err)
	}

	cacheKey := fmt.Sprintf("id-%d", q.ID)
	s.singleCache.Set(cacheKey, q)

	return q, nil
}

func (s *service) Delete(ctx context.Context, id int32) error {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return apperror.Unauthorized("admin access is required to delete question", err)
	}

	cacheKey := fmt.Sprintf("id-%d", id)
	s.singleCache.Delete(cacheKey)
	s.listCache.Flush()

	err = s.questionStore.Delete(ctx, id)
	if err != nil {
		msg := fmt.Sprintf("unable to delete question with id = %d", id)
		return apperror.Internal(msg, err)
	}

	return nil
}

func NewService(logger *logrus.Entry, questionStore Store) (Service, error) {
	return &service{
		questionStore: questionStore,
		logger:        logger,
		singleCache:   cachestore.NewMemoryCache(),
		listCache:     cachestore.NewMemoryCache(),
	}, nil
}
