// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package question

import (
	"context"

	"github.com/sirupsen/logrus"
	"zombiezen.com/go/sqlite"
	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/datastore"
	"gitlab.com/testycool/server/internal/format"
)

type sqliteStore struct {
	name   string
	logger *logrus.Entry
	dbPool *sqlitex.Pool
}

func (s *sqliteStore) Save(ctx context.Context, q Question) (Question, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into questions (type, content_format, content, exam_id)" +
		" values ($type, $content_format, $content, $exam_id)" +
		" returning id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Question{}, datastore.SaveError(s.name, "unable to prepare statement", err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$type", int64(q.Type))
	stmt.SetInt64("$content_format", int64(q.ContentFormat))
	stmt.SetText("$content", q.Content)
	stmt.SetInt64("$exam_id", int64(q.ExamID))

	hasRow, err := stmt.Step()
	if err != nil {
		return Question{}, datastore.SaveError(s.name, "unable to execute statement", err)
	}
	if !hasRow {
		return Question{}, datastore.SaveError(s.name, "unable to get saved data", datastore.ErrNoRows)
	}

	q.ID = int32(stmt.ColumnInt32(0))

	return q, nil
}

func (s *sqliteStore) GetByID(ctx context.Context, id int32) (Question, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, type, content_format, content, exam_id from questions where id = $id limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Question{}, datastore.GetByIDError(s.name, "unable to prepare statement", id, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$id", int64(id))

	hasRow, err := stmt.Step()
	if err != nil {
		return Question{}, datastore.GetByIDError(s.name, "unable to execute statement", id, err)
	}
	if !hasRow {
		return Question{}, datastore.GetByIDError(s.name, "question not found", id, datastore.ErrNoRows)
	}

	return s.questionFromStmt(stmt), nil
}

func (s *sqliteStore) List(ctx context.Context, offset int32, limit int32, f Filter) ([]Question, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	stmt, err := s.buildListStmt(conn, offset, limit, f)
	if err != nil {
		return nil, err
	}
	defer s.resetAndClearStmt(stmt)

	var qq []Question
	var hasRow bool
	for {
		hasRow, err = stmt.Step()
		if err != nil || !hasRow {
			break
		}
		qq = append(qq, s.questionFromStmt(stmt))
	}
	if err != nil {
		return nil, datastore.ListError(s.name, "unable to execute statement", offset, limit, f, err)
	}

	return qq, nil
}

func (s *sqliteStore) GetTotalSize(ctx context.Context, f Filter) (int32, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	stmt, err := s.buildTotalSizeStmt(conn, f)
	if err != nil {
		return 0, err
	}
	defer s.resetAndClearStmt(stmt)

	hasRow, err := stmt.Step()
	if err != nil {
		return 0, datastore.TotalSizeError(s.name, "unable to execute statement", f, err)
	}
	if !hasRow {
		return 0, datastore.TotalSizeError(s.name, "unable to get total size", f, datastore.ErrNoRows)
	}

	totalSize := stmt.GetInt64("total_size")

	return int32(totalSize), nil
}

func (s *sqliteStore) Update(ctx context.Context, q Question) (Question, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "update questions set" +
		" type = $type, content_format = $content_format, content = $content, exam_id = $exam_id" +
		" where id = $id" +
		" returning id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Question{}, datastore.UpdateError(s.name, "unable to prepare statement", q.ID, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$type", int64(q.Type))
	stmt.SetInt64("$content_format", int64(q.ContentFormat))
	stmt.SetText("$content", q.Content)
	stmt.SetInt64("$exam_id", int64(q.ExamID))
	stmt.SetInt64("$id", int64(q.ID))

	hasRow, err := stmt.Step()
	if err != nil {
		return Question{}, datastore.UpdateError(s.name, "unable to execute statement", q.ID, err)
	}
	if !hasRow {
		return Question{}, datastore.UpdateError(s.name, "question not found", q.ID, datastore.ErrNoRows)
	}

	return q, nil
}

func (s *sqliteStore) Delete(ctx context.Context, id int32) error {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "delete from questions where id = $id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return datastore.DeleteError(s.name, "unable to prepare statement", id, err)
	}
	defer s.resetAndClearStmt(stmt)

	stmt.SetInt64("$id", int64(id))

	_, err = stmt.Step()
	if err != nil {
		return datastore.DeleteError(s.name, "unable to execute statement", id, err)
	}

	return nil
}

func (s *sqliteStore) resetAndClearStmt(stmt *sqlite.Stmt) {
	err := stmt.Reset()
	if err != nil {
		s.logger.Warningf("unable to reset statement: %v", err)
	}

	err = stmt.ClearBindings()
	if err != nil {
		s.logger.Warningf("unable to clear statement bindings: %v", err)
	}
}

func (s *sqliteStore) questionFromStmt(stmt *sqlite.Stmt) Question {
	return Question{
		ID:            int32(stmt.GetInt64("id")),
		Type:          Type(stmt.GetInt64("type")),
		ContentFormat: format.Text(stmt.GetInt64("content_format")),
		Content:       stmt.GetText("content"),
		ExamID:        int32(stmt.GetInt64("exam_id")),
	}
}

func (s *sqliteStore) buildListStmt(conn *sqlite.Conn, offset int32, limit int32, f Filter) (*sqlite.Stmt, error) {
	query := "select id, type, content_format, content, exam_id from questions"

	if f.ExamID != 0 {
		query += " where exam_id = $exam_id"
	}
	if limit != 0 {
		query += " limit $limit"
	}
	if offset != 0 {
		if limit == 0 {
			query += " limit -1"
		}
		query += " offset $offset"
	}

	query += ";"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, datastore.ListError(s.name, "unable to prepare statement", offset, limit, f, err)
	}

	if f.ExamID != 0 {
		stmt.SetInt64("$exam_id", int64(f.ExamID))
	}
	if offset != 0 {
		stmt.SetInt64("$offset", int64(offset))
	}
	if limit != 0 {
		stmt.SetInt64("$limit", int64(limit))
	}

	return stmt, nil
}

func (s *sqliteStore) buildTotalSizeStmt(conn *sqlite.Conn, f Filter) (*sqlite.Stmt, error) {
	query := "select count(*) as total_size from questions"

	if f.ExamID != 0 {
		query += " where exam_id = $exam_id"
	}

	query += ";"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, datastore.TotalSizeError(s.name, "unable to prepare statement", f, err)
	}

	if f.ExamID != 0 {
		stmt.SetInt64("$exam_id", int64(f.ExamID))
	}

	return stmt, nil
}

func NewSQLiteStore(logger *logrus.Logger, dbPool *sqlitex.Pool) (Store, error) {
	const name = "question sqlite"
	return &sqliteStore{
		name:   name,
		logger: logger.WithField("store", name),
		dbPool: dbPool,
	}, nil
}
