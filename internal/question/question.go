// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package question

import (
	"errors"
	"fmt"

	"gitlab.com/testycool/server/internal/format"
)

var (
	ErrInvalidID      = errors.New("invalid question id")
	ErrInvalidContent = errors.New("invalid question content")
	ErrInvalidType    = errors.New("invalid question type")
	ErrInvalidExamID  = errors.New("invalid exam id")
)

// Type is the type of the question.
type Type int8

const (
	TypeUnknown Type = iota
	TypeMultipleChoice
	TypeEssay
)

func (t Type) String() string {
	switch t {
	case TypeMultipleChoice:
		return "MULTIPLE_CHOICE"
	case TypeEssay:
		return "ESSAY"
	default:
		return "UNKNOWN"
	}
}

func (t Type) Valid() bool {
	switch t {
	case TypeMultipleChoice, TypeEssay:
		return true
	default:
		return false
	}
}

// Question is a question in an exam.
type Question struct {
	ID            int32       `json:"id,omitempty"`
	Type          Type        `json:"type,omitempty"`
	ContentFormat format.Text `json:"content_format,omitempty"`
	Content       string      `json:"content,omitempty"`
	ExamID        int32       `json:"exam_id,omitempty"`
}

// Validate checks if the question is valid.
func (q *Question) Validate() error {
	if q.ID < 0 {
		return ErrInvalidID
	}

	if q.Content == "" {
		return ErrInvalidContent
	}

	if !q.Type.Valid() {
		return ErrInvalidType
	}

	if q.ExamID < 0 {
		return ErrInvalidExamID
	}

	return nil
}

// ValidateNew validates a new question.
func (q *Question) ValidateNew() error {
	if q.ID != 0 {
		return ErrInvalidID
	}

	if q.Content == "" {
		return ErrInvalidContent
	}

	if !q.Type.Valid() {
		return ErrInvalidType
	}

	if q.ExamID < 0 {
		return ErrInvalidExamID
	}

	return nil
}

// Filter is a filter for listing questions.
type Filter struct {
	ExamID int32 `json:"exam_id,omitempty"`
}

// ListOptions is a list options for listing questions with pagination. Nil filter are ignored.
type ListOptions struct {
	Filter Filter `json:"filter,omitempty"`
	Size   int32  `json:"size,omitempty"`
	Page   int32  `json:"limit,omitempty"`
}

func (o *ListOptions) Validate() error {
	if o.Size < -1 {
		return fmt.Errorf("invalid page size (%d < -1)", o.Size)
	}

	if o.Page < 1 {
		return fmt.Errorf("invalid page number (%d < 1)", o.Page)
	}

	return nil
}

// ListData is the data returned by the list operation.
type ListData struct {
	// Questions is the list of questions returned from the operation.
	Questions []Question `json:"questions,omitempty"`
	// Total is the total number of questions available in the store.
	TotalSize int32 `json:"total_size,omitempty"`
}
