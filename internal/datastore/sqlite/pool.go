// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package sqlite

import (
	"fmt"
	"path/filepath"

	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/config"
)

func NewPool(cfg *config.Config) (*sqlitex.Pool, error) {
	dbPath := filepath.Join(cfg.Runtime.DataDir, cfg.SQLite.DBFileName)
	dbPool, err := sqlitex.Open("file:"+dbPath, 0, cfg.SQLite.PoolSize)
	if err != nil {
		return nil, fmt.Errorf("unable to create sqlite connection pool: %w", err)
	}
	return dbPool, nil
}