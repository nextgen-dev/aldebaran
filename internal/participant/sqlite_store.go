// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package participant

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"zombiezen.com/go/sqlite"
	"zombiezen.com/go/sqlite/sqlitex"

	"gitlab.com/testycool/server/internal/datastore"
)

type sqliteStore struct {
	logger *logrus.Logger
	dbPool *sqlitex.Pool
}

func (s *sqliteStore) Save(ctx context.Context, p Participant) (Participant, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "insert into participants (code, name, exam_id)" +
		" values($code, $name, $exam_id)" +
		" returning id, code, name, exam_id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Participant{}, fmt.Errorf("participant sqliteStore: unable to save participant: %w", err)
	}
	stmt.SetText("$code", p.Code)
	stmt.SetText("$name", p.Name)
	stmt.SetInt64("$exam_id", int64(p.ExamID))

	hasRow, err := stmt.Step()
	if err != nil {
		return Participant{}, fmt.Errorf("participant sqliteStore: unable to save participant: %w", err)
	}
	if !hasRow {
		return Participant{}, fmt.Errorf(
			"participant sqliteStore: unable to get newly saved participant: %w",
			datastore.ErrNoRows,
		)
	}

	p = s.participantFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Participant{}, fmt.Errorf("participant sqliteStore: unable to finalize save: %w", err)
	}

	return p, nil
}

func (s *sqliteStore) GetByID(ctx context.Context, id int32) (Participant, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, code, name, exam_id from participants where id = $id limit 1;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Participant{}, fmt.Errorf("participant sqliteStore: unable to get participant with id %d: %w", id, err)
	}
	stmt.SetInt64("$id", int64(id))

	hasRow, err := stmt.Step()
	if err != nil {
		return Participant{}, fmt.Errorf("participant sqliteStore: unable to get participant with id %d: %w", id, err)
	}
	if !hasRow {
		return Participant{}, fmt.Errorf(
			"participant sqliteStore: unable to get participant with id %d: %w",
			id,
			datastore.ErrNoRows,
		)
	}

	p := s.participantFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Participant{}, fmt.Errorf("participant sqliteStore: unable to get participant with id %d: %w", id, err)
	}

	return p, nil
}

func (s *sqliteStore) ListAll(ctx context.Context) ([]Participant, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, code, name, exam_id from participants;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf("participant sqliteStore: unable to list all participants: %w", err)
	}

	return s.execListStmt(stmt)
}

func (s *sqliteStore) ListAllByExamID(ctx context.Context, examID int32) ([]Participant, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, code, name, exam_id from participants where exam_id = $exam_id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf(
			"participant sqliteStore: unable to list participants with exam_id = %d: %w",
			examID,
			err,
		)
	}
	stmt.SetInt64("$exam_id", int64(examID))

	return s.execListStmt(stmt)
}

func (s *sqliteStore) ListPaginated(ctx context.Context, offset int32, limit int32) ([]Participant, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select id, code, name, exam_id from participants limit $limit offset $offset;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return nil, fmt.Errorf(
			"participant sqliteStore: unable to prepare list participants query with offset = %d and limit = %d: %w",
			offset,
			limit,
			err,
		)
	}
	stmt.SetInt64("$limit", int64(limit))
	stmt.SetInt64("$offset", int64(offset))

	return s.execListStmt(stmt)
}

func (s *sqliteStore) GetTotalSize(ctx context.Context) (int32, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "select count(*) as total_size from participants;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return 0, fmt.Errorf("participant sqliteStore: unable to prepare total size query: %w", err)
	}

	hasRow, err := stmt.Step()
	if err != nil {
		return 0, fmt.Errorf("participant sqliteStore: unable to execute get total size statement: %w", err)
	}
	if !hasRow {
		return 0, fmt.Errorf(
			"participant sqliteStore: total size query returned no rows: %w",
			datastore.ErrNoRows,
		)
	}

	totalSize := stmt.GetInt64("total_size")

	err = stmt.Finalize()
	if err != nil {
		return 0, fmt.Errorf("participant sqliteStore: unable to finalize get total size statement: %w", err)
	}

	return int32(totalSize), nil
}

func (s *sqliteStore) Update(ctx context.Context, p Participant) (Participant, error) {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "update participants set " +
		" code = $code, name = $name, exam_id = $exam_id" +
		" where id = $id returning id, code, name, exam_id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return Participant{}, fmt.Errorf(
			"participant sqliteStore: unable to update participant with id %d: %w",
			p.ID,
			err,
		)
	}

	stmt.SetText("$code", p.Code)
	stmt.SetText("$name", p.Name)
	stmt.SetInt64("$exam_id", int64(p.ExamID))
	stmt.SetInt64("$id", int64(p.ID))

	hasRow, err := stmt.Step()
	if err != nil {
		if code := sqlite.ErrCode(err); code == sqlite.ResultConstraintUnique {
			return Participant{}, datastore.ErrNotUnique
		}
		return Participant{}, fmt.Errorf(
			"participant sqliteStore: unable to update participant with id %d: %w",
			p.ID,
			err,
		)
	}
	if !hasRow {
		return Participant{}, fmt.Errorf(
			"participant sqliteStore: unable to get updated participant with id %d: %w",
			p.ID,
			datastore.ErrNoRows,
		)
	}

	p = s.participantFromStmt(stmt)
	err = stmt.Finalize()
	if err != nil {
		return Participant{}, fmt.Errorf(
			"participant sqliteStore: unable to finalize update participant for id %d: %w",
			p.ID,
			err,
		)
	}

	return p, nil
}

func (s *sqliteStore) Delete(ctx context.Context, id int32) error {
	conn := s.dbPool.Get(ctx)
	defer s.dbPool.Put(conn)

	const query = "delete from participants where id = $id;"

	stmt, err := conn.Prepare(query)
	if err != nil {
		return fmt.Errorf("participant sqliteStore: unable to delete participant with id %d: %w", id, err)
	}

	stmt.SetInt64("$id", int64(id))

	_, err = stmt.Step()
	if err != nil {
		return fmt.Errorf("participant sqliteStore: unable to delete participant with id %d: %w", id, err)
	}

	err = stmt.Finalize()
	if err != nil {
		return fmt.Errorf(
			"participant sqliteStore: unable to finalize delete for participant with id %d: %w",
			id,
			err,
		)
	}

	return nil
}

func (s *sqliteStore) participantFromStmt(stmt *sqlite.Stmt) Participant {
	return Participant{
		ID:     int32(stmt.GetInt64("id")),
		Code:   stmt.GetText("code"),
		Name:   stmt.GetText("name"),
		ExamID: int32(stmt.GetInt64("exam_id")),
	}
}

func (s *sqliteStore) execListStmt(stmt *sqlite.Stmt) ([]Participant, error) {
	var participants []Participant
	for {
		hasRow, err := stmt.Step()
		if err != nil {
			return nil, fmt.Errorf("participant sqliteStore: unable to execute list participants statement: %w", err)
		}
		if !hasRow {
			break
		}

		participants = append(participants, s.participantFromStmt(stmt))
	}

	err := stmt.Finalize()
	if err != nil {
		return nil, fmt.Errorf("participant sqliteStore: unable to finalize list participants statement: %w", err)
	}

	return participants, nil
}

func NewSQLiteStore(logger *logrus.Logger, dbPool *sqlitex.Pool) (Store, error) {
	return &sqliteStore{
		logger: logger,
		dbPool: dbPool,
	}, nil
}
