// Copyright 2021 TestyCool Authors
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package participant

import (
	"context"
	"errors"
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/testycool/server/internal/apperror"
	"gitlab.com/testycool/server/internal/auth"
	"gitlab.com/testycool/server/internal/datastore"
)

type ListFilter struct {
	Name   *string
	Code   *string
	ExamID *int32
}

// Service describes the interface of a service that contains business logic for participants.
type Service interface {
	// Save saves a participant to the store.
	Save(ctx context.Context, p Participant) (Participant, error)
	// GetByID returns a participant by its ID.
	GetByID(ctx context.Context, id int32) (Participant, error)
	// List returns a list of participants paginated by the given parameters.
	// size is the number of participants to return. page is the page number.
	// Returns a list of participants and the total number of participants.
	List(ctx context.Context, size int32, page int32, filter ListFilter) ([]Participant, int32, error)
	// Update updates a participant in the store.
	Update(ctx context.Context, p Participant) (Participant, error)
	// Delete deletes a participant from the store by its ID.
	Delete(ctx context.Context, id int32) error
}

// service is a concrete implementation of the Service interface.
type service struct {
	logger           *logrus.Logger
	participantStore Store
}

func (s *service) Save(ctx context.Context, p Participant) (Participant, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Participant{}, apperror.Unauthorized("admin access is required to create new participant", err)
	}
	err = p.ValidateNonGenerated()
	if err != nil {
		return Participant{}, apperror.InvalidData("new participant data is invalid: "+err.Error(), err)
	}

	p, err = s.participantStore.Save(ctx, p)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			return Participant{}, apperror.Internal("unable get newly created participant", err)
		}
		return Participant{}, apperror.Internal("unable to save participant", err)
	}

	return p, nil
}

func (s *service) GetByID(ctx context.Context, id int32) (Participant, error) {
	// first, check if the request comes from an admin
	err := auth.CheckAdmin(ctx)
	if err != nil {
		// if not admin, check if user is participant by getting its user ID.
		// participant can only get their own data.
		id, err = auth.GetUserID(ctx)
		if err != nil {
			// request is not from admin nor participant, return error.
			return Participant{}, apperror.Unauthenticated("authentication required to get participant data", err)
		}
	}

	p, err := s.participantStore.GetByID(ctx, id)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("participant with id = %d not found", id)
			return Participant{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to get participant with id = %d", id)
		return Participant{}, apperror.Internal(msg, err)
	}

	return p, nil
}

func (s *service) List(ctx context.Context, size int32, page int32, filter ListFilter) ([]Participant, int32, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return nil, 0, apperror.Unauthorized("admin access is required to list participants", err)
	}

	if size < -1 {
		msg := fmt.Sprintf("invalid page size (%d < -1)", size)
		return nil, 0, apperror.OutOfRange(msg, nil)
	}

	if page < 0 {
		msg := fmt.Sprintf("invalid page number (%d < 1)", page)
		return nil, 0, apperror.OutOfRange(msg, nil)
	}

	if filter.ExamID != nil {
		participants, err := s.participantStore.ListAllByExamID(ctx, *filter.ExamID)
		if err != nil {
			msg := fmt.Sprintf("unable to list participants with exam id = %d", *filter.ExamID)
			return nil, 0, apperror.Internal(msg, err)
		}
		return participants, int32(len(participants)), nil
	}

	if size == -1 {
		participants, err := s.participantStore.ListAll(ctx)
		if err != nil {
			return nil, 0, apperror.Internal("unable to list participants", err)
		}
		return participants, int32(len(participants)), nil
	}

	totalSize, err := s.participantStore.GetTotalSize(ctx)
	if err != nil {
		return nil, 0, apperror.Internal("unable to get total size while getting exam list", err)
	}

	offset := size * (page - 1)
	participants, err := s.participantStore.ListPaginated(ctx, offset, size)
	if err != nil {
		return nil, 0, apperror.Internal("unable to list participants", err)
	}

	if participants == nil {
		participants = make([]Participant, 0)
	}

	return participants, totalSize, nil
}

func (s *service) Update(ctx context.Context, p Participant) (Participant, error) {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return Participant{}, apperror.Unauthorized("admin access is required to update participant", err)
	}

	if p.ID == 0 {
		return Participant{}, apperror.InvalidData("id is required to update participant", nil)
	}

	err = p.ValidateNonGenerated()
	if err != nil {
		msg := fmt.Sprintf("update data for participant with id = %d is invalid: %v", p.ID, err)
		return Participant{}, apperror.InvalidData(msg, err)
	}

	p, err = s.participantStore.Update(ctx, p)
	if err != nil {
		if errors.Is(err, datastore.ErrNoRows) {
			msg := fmt.Sprintf("participant with id = %d not found", p.ID)
			return Participant{}, apperror.NotFound(msg, err)
		}
		msg := fmt.Sprintf("unable to update participant with id = %d", p.ID)
		return Participant{}, apperror.Internal(msg, err)
	}

	return p, nil
}

func (s *service) Delete(ctx context.Context, id int32) error {
	err := auth.CheckAdmin(ctx)
	if err != nil {
		return apperror.Unauthorized("admin access is required to delete participant", err)
	}

	err = s.participantStore.Delete(ctx, id)
	if err != nil {
		msg := fmt.Sprintf("unable to delete participant with id = %d", id)
		return apperror.Internal(msg, err)
	}

	return nil
}

// NewService creates a new service with a concrete implementation.
func NewService(logger *logrus.Logger, participantStore Store) (Service, error) {
	return &service{
		logger:           logger,
		participantStore: participantStore,
	}, nil
}
